package uk.mksmart.controllers;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import uk.mksmart.analysis.PredictedSM;


@RestController
public class AnalysisController {

	private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    /*@RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }*/
	
	
	
	
	/*@Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/test").setViewName("test");
    }*/
	
	
	/*@RequestMapping(value="/", method=RequestMethod.GET)
	public String showPage() {
        return "test";
	}*/
	
	
	
	
	@RequestMapping(value="/beta3h", method=RequestMethod.GET)
	public String beta3h() {
        return PredictedSM.getBeta3h();
    }
	
	@RequestMapping(value="/beta24h", method=RequestMethod.GET)
	public String beta24h() {
        return PredictedSM.getBeta24h();
    }
	
	@RequestMapping(value="/beta24hET", method=RequestMethod.GET)
	public String beta24hET() {
        return PredictedSM.getBeta24hET();
	}
	
	@RequestMapping(value="/smartcalendar", method=RequestMethod.GET)
	public String smartcalendar() {
        return PredictedSM.getSmartCalendar();
    }
	
	
	@RequestMapping(value="/smartcalendarHistory", method=RequestMethod.GET)
	public String smartcalendarHistory() {
        return PredictedSM.getSmartCalendarHistory();
    }
	
	
	@RequestMapping(value="/compute3h", method=RequestMethod.GET)
	public String analysis3h() {
        return PredictedSM.getPreSMJason3h();
    }
	
	@RequestMapping(value="/compute24h", method=RequestMethod.GET)
	public String analysis24h() {
        return PredictedSM.getPreSMJason24h();
    }
	
	@RequestMapping(value="/compute24hET", method=RequestMethod.GET)
	public String analysis24hET() {
        return PredictedSM.getPreSMJason24hET();
    }
	
	


	
}
