package uk.mksmart.controllers;


import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Controller
public class PlotController extends WebMvcConfigurerAdapter {

    

    /*@RequestMapping(value="/", method=RequestMethod.POST)
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }*/
    
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/test").setViewName("test");
    }
    
   
  
    @RequestMapping(value="/", method=RequestMethod.GET)
	public String showPage() {
        return "test";
	}
    
    
    @RequestMapping(value="/analysis", method=RequestMethod.GET)
   	public String showPageAnalysis() {
           return "analysis";
   	}

    
   
   /* @RequestMapping(value="/greeting", method=RequestMethod.GET)
	public Greeting analysis(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
    	return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }*/
    
    
    
    /*@RequestMapping("/")
    public void mainPage() {
    	
    }*/
    
}
