package uk.mksmart.analysis;


import org.apache.commons.lang.ArrayUtils;

import com.google.gson.Gson;

import uk.mksmart.gateways.data.Data;
import uk.mksmart.gateways.data.forecast.ForecastData;
import uk.mksmart.gateways.data.forecast.ForecastDataPeriod;
import uk.mksmart.gateways.data.historical.HistoricalData;
import uk.mksmart.gateways.data.historical.HistoricalDataPeriod;
import uk.mksmart.main.GardenWateringApplication;

public class PredictedSM {
	
	private static double[] futureSM;
	
	private static double[] futureSMHistory;
	
	private static double[] preSM3h;
	private static double[] beta3h;
	private static double[] sm3h;//y -- sm day after
	private static double[][] vars3h;//xi
	private static HistoricalData history3h;
	
	private static double[] preSM24h;
	private static double[] beta24h;
	private static double[] sm24h; //y -- sm day after
	private static double[][] vars24h;//xi
	private static HistoricalData history24h;
	
	private static double[] preSM24hET;
	private static double[] beta24hET;
	private static double[] sm24hET;//y -- sm day after
	private static double[][] vars24hET;//xi
	private static HistoricalData history24hET;
	
	private static boolean intercept;
	
	private static ForecastData smartCalendar = new ForecastData();
	private static ForecastData smartCalendarHistory = new ForecastData();
	
	
	public PredictedSM(HistoricalData history3h, double[] beta3h, HistoricalData history24h, double[] beta24h, HistoricalData history24hET, double[] beta24hET, boolean intercept) {
		
		this.intercept = intercept;
		
		this.history3h = history3h;
		this.sm3h = history3h.getYValue();
		this.vars3h = history3h.getXValues();
		this.preSM3h = new double[sm3h.length];
		this.beta3h = beta3h;
		
		
		this.history24h = history24h;
		this.sm24h = history24h.getYValue();
		this.vars24h = history24h.getXValues();
		this.preSM24h = new double[sm24h.length];
		this.beta24h = beta24h;
	
		
		this.history24hET = history24hET;
		this.sm24hET = history24hET.getYValue();
		this.vars24hET = history24hET.getXValues();
		this.preSM24hET = new double[sm24hET.length];
		this.beta24hET = beta24hET;
				
		System.out.println("main");
		
		
		
		//calculateHistoricalPreSM();
	}
	
	
	
	public void calculateFuturePreSM(Double currentSM, ForecastData list) {
		/**
		 * 1. calcolare il SM per domani
		 * 2. prendere il sm di domani e calcolare quello di dopo domani, etc..
		 */
		//current day
		futureSM = new double[list.size()];
		
		
		for(int i=0; i<list.size(); i++) {
			futureSM[i] = currentSM;
			System.out.println("---------------------------------------------------------------------");
			double tempSM = 0;
			ForecastDataPeriod item = list.get(i);
			
			for(int j =0; j < beta24h.length; j++) {//ultimo valore temperatura day che non uso per calcolare BETA
				//TODO discorso intercetta, qui la considero zero
				
				if(j==1) { //serve SM attuale!!!
					System.out.println("currentSM " + currentSM + " * "  + beta24h[j]);
					tempSM += currentSM * beta24h[j];
					item.set(j, new Data("SM(real)", currentSM));
					//tempSM += currentSM * beta24h[j];
				} else {
								
					
					System.out.println(item.get(j).getName() + " " + item.get(j).getValue() + " * " + beta24h[j]);

					
					tempSM += ((Double)item.get(j).getValue() * beta24h[j]);
				}
				
			
			}
			
			currentSM = tempSM;
		}
		
		for(int i=0; i< futureSM.length; i++) {
			System.out.println("valore " + futureSM[i]);
		}
		
		
		this.smartCalendar = list;
	}
	
	
	
	
	
	/**
	 * Calculate the predicted SM applying the defined model using forecast data
	 */
	public void calculateFuturePreSM(Double currentSM, ForecastData list, boolean fasle) {
		/**
		 * 1. calcolare il SM per domani
		 * 2. prendere il sm di domani e calcolare quello di dopo domani, etc..
		 */
		//current day
		futureSMHistory = new double[list.size()];
		
		
		for(int i=0; i<list.size(); i++) {
			futureSMHistory[i] = currentSM;
			System.out.println("---------------------------------------------------------------------");
			double tempSM = 0;
			ForecastDataPeriod item = list.get(i);
			
			for(int j =0; j < beta24h.length; j++) {//ultimo valore temperatura day che non uso per calcolare BETA
				//TODO discorso intercetta, qui la considero zero
				
				if(j==1) { //serve SM attuale!!!
					System.out.println("currentSM " + currentSM + " * "  + beta24h[j]);
					tempSM += currentSM * beta24h[j];
					item.set(j, new Data("SM(real)", currentSM));
					//tempSM += currentSM * beta24h[j];
				} else {
								
					
					System.out.println(item.get(j).getName() + " " + item.get(j).getValue() + " * " + beta24h[j]);

					
					tempSM += ((Double)item.get(j).getValue() * beta24h[j]);
				}
				
			
			}
			
			currentSM = tempSM;
		}
		
		for(int i=0; i< futureSMHistory.length; i++) {
			System.out.println("valore " + futureSMHistory[i]);
		}
		
		
		this.smartCalendarHistory = list;
	}
	
	
	/**
	 * Calculate the predicted SM applying the defined model using historical data
	 */
	public void calculateHistoricalPreSM() {
		
		System.out.println("Calculating y for historical data .... ");
		for(int i=0; i<vars3h.length; i++) {
			double vars_i[] = vars3h[i];
			//double[] preSM_i = new double[vars_i.length];
			double preSMi = 0;
			int intercPos = 0;
			if(!intercept) {
				preSMi = beta3h[0];
				intercPos = 1;
			}
			
			
			for(int j=0; j<vars_i.length; j++) {
				
				
				preSMi += (vars_i[j] * beta3h[j+intercPos]);
				
			}
			
			preSM3h[i] = preSMi;
			
		}
		
		
		for(int i=0; i<vars24h.length; i++) {
			double vars_i[] = vars24h[i];
			//double[] preSM_i = new double[vars_i.length];
			double preSMi = 0;
			int intercPos = 0;
			if(!intercept) {
				preSMi = beta24h[0];
				intercPos = 1;
			}
			
			for(int j=0; j<vars_i.length; j++) {
				
				preSMi += (vars_i[j] * beta24h[j+intercPos]);
				
			}
			
			preSM24h[i] = preSMi;
			
		}
		
		
		for(int i=0; i<vars24hET.length; i++) {
			double vars_i[] = vars24hET[i];
			//double[] preSM_i = new double[vars_i.length];
			double preSMi = 0;
			int intercPos = 0;
			
			if(!intercept) {
				preSMi = beta24hET[0];
				intercPos = 1;
			}
			
			for(int j=0; j<vars_i.length; j++) {
				
				preSMi += (vars_i[j] * beta24hET[j+intercPos]);
				
			}
			
			preSM24hET[i] = preSMi;
			
		}
		
		
		System.out.println("Finished!!");

	}
	
	
	public static String getBeta3h() {
		
		String descr = "";
		int intercPos = 0;
		if(!intercept) {
			descr = String.valueOf(beta3h[0]) + " + ";
			intercPos = 1;
		}
		
	
		HistoricalDataPeriod hdp = history3h.get(0);
		
		for(int i=0; i< hdp.getXSize(); i++ ) {
			
			/*if(i == 0 || i == 1 || i == 4  || i == 5 || i == 11) {
    			continue;
	    	} else {
	    		if(j != 0 && i != j) {
					descr += " + ";
				}
	    		
	    		descr += history3h.getDescription()[i] + " * "+ beta3h[j] + " ";
	    		j++;
	    	}*/
			
			descr += hdp.get(i).getName() + " * "+ beta3h[i+intercPos] + " ";
			if(i != hdp.getXSize()) {
				descr += " + ";
			}
		}
		//return " Rainfall * "+ beta[0] + " + SM * " + beta[1] + " + WIND speed * " + beta[2] + " + max temp * " + beta[3] + " + pressure * " + beta[4] + " + humidity * " + beta[5] + " + rainfall * " + beta[6];
		return descr;
	}
	
	public static String getBeta24h() {
		
		String descr = "";
		int intercPos = 0;
		if(!intercept) {
			descr = String.valueOf(beta24h[0]) + " + ";
			intercPos = 1;
		}
		
		HistoricalDataPeriod hdp = history24h.get(0);
		
		for(int i=0; i< hdp.getXSize(); i++ ) {
			
			/*if(i == 0 || i == 1 || i == 4  || i == 5 || i == 11) {
    			continue;
	    	} else {
	    		if(j != 0 && i != j) {
					descr += " + ";
				}
	    		
	    		descr += history3h.getDescription()[i] + " * "+ beta3h[j] + " ";
	    		j++;
	    	}*/
			//System.out.println(hdp.get(i).getName());
			descr += hdp.get(i).getName() + " * "+ beta24h[i+intercPos] + " ";
		}
		//return " Rainfall * "+ beta[0] + " + SM * " + beta[1] + " + WIND speed * " + beta[2] + " + max temp * " + beta[3] + " + pressure * " + beta[4] + " + humidity * " + beta[5] + " + rainfall * " + beta[6];
		return descr;
	}
	
	
	/**
	 * considering ET
	 * @return
	 */
	public static String getBeta24hET() {
		
		
		String descr = "";
		int intercPos = 0;
		if(!intercept) {
			descr = String.valueOf(beta24hET[0]) + " + ";
			intercPos = 1;
		}
		
		
		
		HistoricalDataPeriod hdp = history24hET.get(0);
		
		for(int i=0; i< hdp.getXSize(); i++ ) {
			
			/*if(i == 0 || i == 1 || i == 4  || i == 5 || i == 11) {
    			continue;
	    	} else {
	    		if(j != 0 && i != j) {
					descr += " + ";
				}
	    		
	    		descr += history3h.getDescription()[i] + " * "+ beta3h[j] + " ";
	    		j++;
	    	}*/
			//System.out.println(hdp.get(i).getName());
			descr += hdp.get(i).getName() + " * "+ beta24hET[i+intercPos] + " ";
		}
		//return " Rainfall * "+ beta[0] + " + SM * " + beta[1] + " + WIND speed * " + beta[2] + " + max temp * " + beta[3] + " + pressure * " + beta[4] + " + humidity * " + beta[5] + " + rainfall * " + beta[6];
		return descr;
	}

	
	
	
	/*public static String getPreSMJason() {
		
		
			String json = null;
			json = "{";
			
			json += "\"cols\": [{\"id\": \"SM_DA\", \"label\": \"SM_DA\", \"type\": \"number\"}, {\"id\": \"SM_PRE\",\"label\": \"SM_PRE\", \"type\": \"number\"}]}";
			return json;
	}*/
	
	
	public static String getPreSMJason3h() {
		String json = null;
		json = "{";
		
		json += "\"cols\": [{\"id\": \"SM_DA\", \"label\": \"SM_DA\", \"type\": \"number\"}, {\"id\": \"SM_PRE\",\"label\": \"SM_PRE\", \"type\": \"number\"}],";
		
		
		for(int i=0; i < preSM3h.length; i++) {
			//for(int j=0; j < preSM[i].length; j++) {
				
				
				if(i == 0)
					json += "\"rows\": [";
				
				json += "{\"c\":[";
				
				json += "{\"v\": "+ sm3h[i] + "},";
				json += "{\"v\": "+ preSM3h[i] + "}]}";
							
				
				        
			
			if(i != preSM3h.length - 1)
				json += ",";
			else
				json += "";
			
		}
		json += "]}";
		
		//System.out.println("Il mio JSON " + json);
		return json;
	}
	
	
	
	public static String getPreSMJason24h() {
		String json = null;
		json = "{";
		
		json += "\"cols\": [{\"id\": \"SM_DA\", \"label\": \"SM_DA\", \"type\": \"number\"}, {\"id\": \"SM_PRE\",\"label\": \"SM_PRE\", \"type\": \"number\"}],";
		
		
		for(int i=0; i < preSM24h.length; i++) {
			//for(int j=0; j < preSM[i].length; j++) {
				
				
				if(i == 0)
					json += "\"rows\": [";
				
				json += "{\"c\":[";
				
				json += "{\"v\": "+ sm24h[i] + "},";
				json += "{\"v\": "+ preSM24h[i] + "}]}";
							
				
				        
			
			if(i != preSM24h.length - 1)
				json += ",";
			else
				json += "";
			
		}
		json += "]}";
		
		//System.out.println("Il mio JSON " + json);
		return json;
	}
	
	public static String getPreSMJason24hET() {
		String json = null;
		json = "{";
		
		json += "\"cols\": [{\"id\": \"SM_DA\", \"label\": \"SM_DA\", \"type\": \"number\"}, {\"id\": \"SM_PRE\",\"label\": \"SM_PRE\", \"type\": \"number\"}],";
		
		
		for(int i=0; i < preSM24hET.length; i++) {
			//for(int j=0; j < preSM[i].length; j++) {
				
				
				if(i == 0)
					json += "\"rows\": [";
				
				json += "{\"c\":[";
				
				json += "{\"v\": "+ sm24hET[i] + "},";
				
				json += "{\"v\": "+ preSM24hET[i] + "}]}";
							
				
				        
			
			if(i != preSM24hET.length - 1)
				json += ",";
			else
				json += "";
			
		}
		json += "]}";
		
		//System.out.println("Il mio JSON " + json);
		return json;
	}

	public static String getSmartCalendar() {
		// TODO Auto-generated method stub
		String json = new Gson().toJson(smartCalendar);
		System.out.println(json);
		return json;
		
		
	}
	
	public static String getSmartCalendarHistory() {
		// TODO Auto-generated method stub
		String json = new Gson().toJson(smartCalendarHistory);
		System.out.println(json);
		return json;
		
		
	}
	
	
	
	

}
