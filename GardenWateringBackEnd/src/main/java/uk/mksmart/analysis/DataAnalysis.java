package uk.mksmart.analysis;



import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;

import uk.mksmart.gateways.data.historical.HistoricalData;



public class DataAnalysis {
	
	
	/*private File file;
	private FileWriter fstream;
	private BufferedWriter out;*/

	private double[] y3h;
	private double[][] x3h;
	
	private double[] y24h;
	private double[][] x24h;
	
	private double[] y24hET;
	private double[][] x24hET;
	//private int nVar = 0;
	
	private double[] beta3h;
	private double[] beta24h;
	private double[] beta24hET;
	
	private HistoricalData history3h;
	private HistoricalData history24h;
	private HistoricalData history24hET;

	private boolean intercept = true;
	
	
	public DataAnalysis(HistoricalData history3h, HistoricalData history24h, HistoricalData history24hET) {


		OLSMultipleLinearRegression mlr = new OLSMultipleLinearRegression();
		mlr.setNoIntercept(intercept);
		
		
		this.history3h = history3h;
		this.x3h = history3h.getXValues();
		this.y3h = history3h.getYValue();
		//int nVar = x3h[0].length;
		mlr.newSampleData(y3h, x3h);
		
		
		beta3h = mlr.estimateRegressionParameters();
		

		this.history24h = history24h;
		this.x24h = history24h.getXValues();
		this.y24h = history24h.getYValue();
		mlr.newSampleData(y24h, x24h);
		//mlr.setNoIntercept(intercept);
		beta24h = mlr.estimateRegressionParameters();
		


		this.history24hET = history24hET;
		this.x24hET = history24hET.getXValues();
		this.y24hET = history24hET.getYValue();
		//nVar = x24hET[0].length;
		mlr.newSampleData(y24hET, x24hET);
		//mlr.setNoIntercept(intercept);
		beta24hET = mlr.estimateRegressionParameters();
		
		//createLogFile();
		
		
		/*for (int i=0; i < y.length; i++) {
			
			for(int z=0; z<x[0].length; z++)  {
				System.out.print(", " + x[i][z]);
			}
			System.out.print("," + y[i]);
			System.out.println();
		}*/
		

		//NB: I don't know what the first number of beta is
		/*for(int i=0; i<beta.length; i++)
			System.out.println(beta[i]);*/
		
		
		
		//preSM.calculateFutureSM(currentSM);
	}
	
	
	public PredictedSM getPredictedSMObj() {
		PredictedSM preSM = new PredictedSM(history3h, beta3h, history24h, beta24h, history24hET, beta24hET, intercept);
		return preSM;
	}
	
	
	/*private void createLogFile() {
		file = new File("data/valuesForChar.txt");
		file.getParentFile().mkdirs();
		if(file.exists())
			file.delete();
		try {
			file.createNewFile();
			fstream = new FileWriter(file, true);
			out = new BufferedWriter(fstream);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	

}
