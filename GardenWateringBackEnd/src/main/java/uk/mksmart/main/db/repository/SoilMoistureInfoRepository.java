package uk.mksmart.main.db.repository;

import org.springframework.data.repository.CrudRepository;

import uk.mksmart.main.db.SoilMoistureInfo;

public interface SoilMoistureInfoRepository extends CrudRepository<SoilMoistureInfo, String>  {
	

}
