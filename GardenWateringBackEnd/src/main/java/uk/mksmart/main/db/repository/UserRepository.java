package uk.mksmart.main.db.repository;

import org.springframework.data.repository.CrudRepository;

import uk.mksmart.main.db.User;

public interface UserRepository extends CrudRepository<User, String> {

}
