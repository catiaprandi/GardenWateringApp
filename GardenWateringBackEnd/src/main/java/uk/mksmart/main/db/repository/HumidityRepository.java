package uk.mksmart.main.db.repository;

import org.springframework.data.repository.CrudRepository;

import uk.mksmart.main.db.Humidity;

public interface HumidityRepository  extends CrudRepository<Humidity, Long> {

}
