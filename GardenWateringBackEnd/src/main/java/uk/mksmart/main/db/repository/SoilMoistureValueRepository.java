package uk.mksmart.main.db.repository;

import org.springframework.data.repository.CrudRepository;

import uk.mksmart.main.db.SoilMoistureValue;

public interface SoilMoistureValueRepository extends CrudRepository<SoilMoistureValue, Long> {

}
