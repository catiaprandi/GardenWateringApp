package uk.mksmart.main.db;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Humidity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String datatime;
	private String idSMS;
	private double value;
	private int period;
	private String source;
	
	protected Humidity() {}
	
	public Humidity(String datatime, String idSMS, double value, int period, String source) {
		this.datatime = datatime;
		this.idSMS = idSMS;
		this.value = value;
		this.period = period;
		this.source = source;
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDatatime() {
		return datatime;
	}

	public void setDatatime(String datatime) {
		this.datatime = datatime;
	}

	public String getIdSMS() {
		return idSMS;
	}

	public void setIdSMS(String idSMS) {
		this.idSMS = idSMS;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	
	
	
	
}
