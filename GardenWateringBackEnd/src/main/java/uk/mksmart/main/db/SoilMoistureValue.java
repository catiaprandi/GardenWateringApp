package uk.mksmart.main.db;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SoilMoistureValue {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String datatime;
	private String idSMS;
	private double value;
	private int period; //important in the analysis part
	
	
	protected SoilMoistureValue() {}
	
	public SoilMoistureValue(String datatime, String idSMS, double value, int period) {
		this.value = value;
		this.datatime = datatime;
		this.idSMS = idSMS;
		this.period = period;
	}


	
	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public double getValue() {
		return value;
	}


	public void setValue(double value) {
		this.value = value;
	}


	public String getDatatime() {
		return datatime;
	}


	public void setDatatime(String datatime) {
		this.datatime = datatime;
	}

	
	public String getIdSMS() {
		return idSMS;
	}

	public void setIdSMS(String idSMS) {
		this.idSMS = idSMS;
	}

	@Override
    public String toString() {
        return String.format(
                "SoilMoistureVale[id=%d, datatime='%s', idSM='%s', valye='%d']",
                id, datatime, idSMS, value);
    }

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}
	
	
	
}