package uk.mksmart.main.db;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Temperature {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String datatime;
	private String idSMS;
	private int period;
	private String source;
	private double tempMin;
	private double tempMax;
	
	protected Temperature() {}
	
	public Temperature(String datatime, String idSMS, int period, String source, double tempMin, double tempMax) {
		this.datatime = datatime;
		this.idSMS = idSMS;
		this.period = period;
		this.source = source;
		this.tempMin = tempMin;
		this.tempMax = tempMax;
		
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDatatime() {
		return datatime;
	}

	public void setDatatime(String datatime) {
		this.datatime = datatime;
	}

	public String getIdSMS() {
		return idSMS;
	}

	public void setIdSMS(String idSMS) {
		this.idSMS = idSMS;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
}
