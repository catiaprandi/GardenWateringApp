package uk.mksmart.main.db.repository;

import org.springframework.data.repository.CrudRepository;

import uk.mksmart.main.db.Pression;

public interface PressionRepository extends CrudRepository<Pression, Long> {

}
