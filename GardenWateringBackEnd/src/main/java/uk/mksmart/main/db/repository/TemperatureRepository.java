package uk.mksmart.main.db.repository;

import org.springframework.data.repository.CrudRepository;

import uk.mksmart.main.db.Temperature;

public interface TemperatureRepository extends CrudRepository<Temperature, Long> {
	

}
