package uk.mksmart.main.db;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class SoilMoistureInfo {
	
	@Id
	private String id;
	private String userEmail;
	private double latitude;
	private double longitude;
	//to access to the datahub
	private String APIKey;
	private String feed;
	private int dataStream;

	
	protected SoilMoistureInfo() {}
	
	public SoilMoistureInfo(String id, String userEmail, double latitude, double longitude, String APIKey, String feed, int dataStream) {
		this.id = id;
		this.userEmail = userEmail;
		this.latitude = latitude;
		this.longitude = longitude;
		this.APIKey = APIKey;
		this.feed = feed;
		this.dataStream = dataStream;
	}
	
	
	@Override
    public String toString() {
        return String.format(
                "SoilMoistureInfo[id=%d, clientEmail='%s', latitude='%d', longitude='%d']",
                id, userEmail, latitude, longitude);
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getAPIKey() {
		return APIKey;
	}

	public void setAPIKey(String aPIKey) {
		APIKey = aPIKey;
	}

	public String getFeed() {
		return feed;
	}

	public void setFeed(String feed) {
		this.feed = feed;
	}

	public int getDataStream() {
		return dataStream;
	}

	public void setDataStream(int dataStream) {
		this.dataStream = dataStream;
	}
	
	
	
	
}
