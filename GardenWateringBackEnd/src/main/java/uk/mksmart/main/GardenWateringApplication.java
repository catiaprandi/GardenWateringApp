package uk.mksmart.main;

import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import uk.mksmart.analysis.DataAnalysis;
import uk.mksmart.analysis.PredictedSM;
import uk.mksmart.gateways.SoilMoistureGateway;
import uk.mksmart.gateways.data.DataFile;
import uk.mksmart.gateways.data.forecast.ForecastData;
import uk.mksmart.gateways.data.forecast.ManageForecastData;
import uk.mksmart.gateways.data.historical.HistoricalData;
import uk.mksmart.gateways.data.historical.ManageHistoricalData;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = {"uk.mksmart.*"})

public class GardenWateringApplication implements CommandLineRunner {

	
	//@Autowired CustomerRepository repository;
	
	//private static final int HOURS = 3;
	//public static boolean ET = true;
	private static final Logger log = LoggerFactory.getLogger(GardenWateringApplication.class);
	private static ManageHistoricalData mhd;
	
	
	private boolean actualCalendar = false;
	
	public static void main(String args[]) throws ParseException, IOException {
		
		SpringApplication.run(GardenWateringApplication.class);
		
		
	}
	
	@Override
    public void run(String... strings) throws Exception {
		
		mhd = new ManageHistoricalData("Milton Keynes");
		//mhd.getHistoricalData("2015-08-31", "2015-09-07", 3);
		
		//mhd.getHistoricalData("2015-09-06", "2015-09-08", 24);
		
		/*Calendar today = Calendar.getInstance();
		//today.set(Calendar.DAY_OF_MONTH, 0);
		int year = today.get(Calendar.YEAR);
		int month = today.get(Calendar.MONTH);
		int day = today.get(Calendar.DAY_OF_MONTH);
		
		String monthStr;
		String dayStr;
		if(month < 10)
			monthStr = "0" + month;
		else
			monthStr = String.valueOf(month);
		
		if(day < 10)
			dayStr = "0" + day;
		else
			dayStr = String.valueOf(day);
		
		//System.out.println(""+year+"-"+monthStr+"-"+dayStr);*/
		
		
		double currentSM = mhd.getCurrentSM();
		
		//SoilMoistureGateway currentSM = (new SoilMoistureGateway("Milton Keynes")).getCurrentSM();

		
	    ManageForecastData manForcastData = new ManageForecastData("Milton Keynes");
		
	    ForecastData fdActual = new ForecastData();
	    
	    ForecastData fdHistory = new ForecastData();
	    
	    ManageHistoricalData mhdForecast = new ManageHistoricalData("Milton Keynes");
	    
	    //if(actualCalendar == true) {
	    
	    	fdActual = manForcastData.getForecastWeatherData(24);
	    	
	   // } else {
	    	//1428364800
	    	fdHistory = mhdForecast.getHistoricalDataForForecast(1430006400);
	    	//devo prendere i dati storici come se fossero quelli futuri
	    //}
		
		
		DataFile file3h = new DataFile("data/prove/historicalData3.txt");
		HistoricalData history3h = file3h.convertDataInObj();
		history3h.cleanData();
		
		DataFile file24h = new DataFile("data/prove/historicalData24.txt");
		HistoricalData history24h = file24h.convertDataInObj();
		history24h.cleanData();
		
		DataFile file24hET = new DataFile("data/prove/historicalData24.txt", true);
		HistoricalData history24hET = file24hET.convertDataInObj();
		history24hET.cleanData();
		

		DataAnalysis da = new DataAnalysis(history3h, history24h, history24hET);
		PredictedSM preSMonObj = da.getPredictedSMObj();
		
		preSMonObj.calculateHistoricalPreSM();
		
		//if(actualCalendar == true) {
		
			preSMonObj.calculateFuturePreSM(currentSM, fdActual);
			
		//} else {
			preSMonObj.calculateFuturePreSM(60.1098039215686, fdHistory, false);
				
		//}
		
		//PredictedSM.getSmartCalendar();
		
		/*
		// save a couple of customers
        repository.save(new Customer("Jack", "Bauer"));
        repository.save(new Customer("Chloe", "O'Brian"));
        repository.save(new Customer("Kim", "Bauer"));
        repository.save(new Customer("David", "Palmer"));
        repository.save(new Customer("Michelle", "Dessler"));

        // fetch all customers
        System.out.println("Customers found with findAll():");
        System.out.println("-------------------------------");
        for (Customer customer : repository.findAll()) {
            System.out.println(customer);
        }
        System.out.println();

        // fetch an individual customer by ID
        Customer customer = repository.findOne(1L);
        System.out.println("Customer found with findOne(1L):");
        System.out.println("--------------------------------");
        System.out.println(customer);
        System.out.println();

        // fetch customers by last name
        System.out.println("Customer found with findByLastName('Bauer'):");
        System.out.println("--------------------------------------------");
        for (Customer bauer : repository.findByLastName("Bauer")) {
            System.out.println(bauer);
        }*/
        
		
	}

	
}
