package uk.mksmart.gateways;

import java.util.Calendar;

import org.springframework.web.client.RestTemplate;

import uk.mksmart.gateways.data.forecast.WeatherForecastJson;

public class WeatherForecastGateway {
	
	
	
	private final String SOURCE = "http://api.openweathermap.org/data/2.5/forecast/daily"; //?q=Milton Keynes,GB&units=metric&cnt=16&APPID=d4bae
			
	private int cnt = 16;
	
	private String units = "metric";
			
	private final String APPID = "d4bae7baac195853e82a76a8bd489cbf";
	
	private String city;
	private String url;
	
	private RestTemplate restTemplateForecast;
	
	//private static RestTemplate historicalTemplate;
	
	public WeatherForecastGateway(String city) {
		this.city = city;
		
		restTemplateForecast = new RestTemplate();
		
	}
	
	/**
	 *
	 * @param period
	 * @return
	 */
	public WeatherForecastJson getWeatherData(int period) {
		
		
		this.url = SOURCE + "?q=" + city + "&units=" + units +"&cnt=" + this.cnt  + "&APPID=" + APPID; 
		System.out.println("URL " + url);
		
		//forecast = restTemplateForecast.getForObject("http://api.openweathermap.org/data/2.5/forecast/daily?q=Milton Keynes,GB&units=metric&cnt=16&APPID=d4bae7baac195853e82a76a8bd489cbf", WeatherForecast.class);
		WeatherForecastJson forecast  = restTemplateForecast.getForObject(url, WeatherForecastJson.class);
		
		return forecast;
	}
	
	public String getUrl() {
		return url;
	}

	

}
