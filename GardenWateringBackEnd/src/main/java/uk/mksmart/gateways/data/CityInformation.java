package uk.mksmart.gateways.data;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)

public class CityInformation {

	private Long id;
	private String name;
	private Coor coord;
	private String country;
	
	
	public String getName(){
		return name;
	}
}
