package uk.mksmart.gateways.data;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)

public class Coor {
	private Double lon;
	private Double lat;
}
