package uk.mksmart.gateways.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SoilMoistureClass {
	private Date time;
	private Double value;

	public SoilMoistureClass(Date time, Double value) {
		this.time = time;
		this.value = value;
		
	}
	
	/*public SoilMoistureClass(String time, Double value) {
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		Date date = new Date();
		try {
			date = sdf.parse(time);
			this.time = date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.value = value;
		
	}*/

	public Date time() {
		return time;
	}
	
	public Double getValue() {
		return value;
	}
	
	public Date getTime() {
		return time;
		
	}
	
	
	
	
	
}
