package uk.mksmart.gateways.data.historical;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import uk.mksmart.evapotraspiration.City;
import uk.mksmart.evapotraspiration.MiltonKeynes;
import uk.mksmart.evapotraspiration.ReferenceEvapotraspiration;
import uk.mksmart.evapotraspiration.Temperature;
import uk.mksmart.evapotraspiration.WindSpeed;
import uk.mksmart.gateways.RainFallGateway;
import uk.mksmart.gateways.SoilMoistureGateway;
import uk.mksmart.gateways.HistoricalWeatherGateway;
import uk.mksmart.gateways.WeatherForecastGateway;
import uk.mksmart.gateways.data.Data;
import uk.mksmart.gateways.data.RainFallDH;
import uk.mksmart.gateways.data.SoilMoistureClass;
import uk.mksmart.gateways.data.SoilMoistureJson;
import uk.mksmart.gateways.data.SoilMoistureListClass;
import uk.mksmart.gateways.data.forecast.ForecastData;
import uk.mksmart.gateways.data.forecast.ForecastDataPeriod;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.impl.client.HttpClients;


public class ManageHistoricalData {

	
	

	private List<SoilMoistureListClass> historicalSoilMoistures = new ArrayList<SoilMoistureListClass>();

	//private static RainFall[] rainFalls;

	private HistoricalWeatherJson historicalData;

	private List<HistoricalWeatherJson> history = new ArrayList<HistoricalWeatherJson>();
	
	private RainFallDH[] rainFalls;
	
	private SoilMoistureJson[] soilMoistures;
	
	

	private String city;
	
	
	private Date startDate;
	private Date endDate;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	private String timezone = "GTM";
	
	private String time = "00:01:00"; //ATTENZIONE TOLTO T E Z era T00:03:00Z
	
	
	/*the data are related to 'x' hours*/
	private int period;
	
	private final int NUM_IND_VAR = 7;
	

	private ArrayList<ArrayList<Double>> x = new ArrayList<ArrayList<Double>>();
	private ArrayList<Double> y = new ArrayList<Double>();

	//private List<Double> movingAveragesSM = new ArrayList<Double>();

	private File file;
	private FileWriter fstream;
	private BufferedWriter out;
	

	
	/*
	 * get historical data of the previous day
	 */
	/*public void getHistoricalData() throws ParseException, IOException {
		
		dateFormat.setTimeZone(TimeZone.getTimeZone(timezone));
		
		calculateData();
		
	}*/
	
	public ManageHistoricalData(String city) {
		this.city = city;
	}
	
	
	public double getCurrentSM()  {
		SoilMoistureGateway sg = new SoilMoistureGateway(city); 
		SoilMoistureJson[] list = sg.getCurrentSoilMoistureData();
		if (list.length != 0) {
			SoilMoistureListClass historicalSoilMoisturesPeriod = new SoilMoistureListClass(list);
			System.out.println("Valore di soil moisture attuale " + historicalSoilMoisturesPeriod.getLastValue());
			
			return historicalSoilMoisturesPeriod.getLastValue();
		}
		return -1;
	}
	
	
	/*
	 * get historical data from the specified start data to the specified end data
	 */
	public void getHistoricalData(String start, String end, int period) throws ParseException, IOException {
		
		dateFormat.setTimeZone(TimeZone.getTimeZone(timezone));
		//2015-04-07
		startDate = dateFormat.parse(start+"T" + time + "Z");
		//2015-07-30
		endDate = dateFormat.parse(end+"T" + time + "Z");
		
		this.period = period;
		
		
		calculateData();
		
	}
	
	
	
	
	public ForecastData getHistoricalDataForForecast(long start) {
		HistoricalWeatherJson historicalData = new HistoricalWeatherJson();
		
		ForecastData historicalForecastData = new ForecastData();
		
		
		HistoricalWeatherGateway wg = new HistoricalWeatherGateway(city);
		long unixTemp = start;
		for(int i=0; i< 15; i++)  {
			ForecastDataPeriod period = new ForecastDataPeriod();
			
			//chiedo i dati partendo da start
			historicalData = wg.getWeatherDataForForecast(24, unixTemp);
			
			
			historicalData.calculateIntervalMinMax();
			historicalData.calculateIntervalHumidity();
			historicalData.calculateIntervalPressure();
			
			Data rain = new Data("Rainfall", 0.0);
			Data sm = new Data("SM(real)", -1);
			Data speed = new Data("WindSpeed", historicalData.getSpeed());
			Data pressure = new Data("pressure", historicalData.getIntervalPressure());
			Data humidity = new Data("humidity", historicalData.getIntervalHumidity());
			
			Data minTemp = new Data("minTemp", historicalData.getIntervalMINInCelsius());
			Data maxTemp = new Data("maxTemp", historicalData.getIntervalMAXInCelsius());
			
			Data dayTemp = new Data("dayTemp", historicalData.getIntervalMAXInCelsius() - 2);
			
			Data main = new Data("main", historicalData.getDescription());
			Data description = new Data("description", historicalData.getDescription() );
			
			long timeStamp = historicalData.getDT();
			Date time = new Date(timeStamp*1000);
			//System.out.println(time);
			
			Data date = new Data("date", time.toString());
			
			
			period.add(rain);
			period.add(sm);
			period.add(speed);
			period.add(minTemp);
			period.add(maxTemp);
			period.add(pressure);
			period.add(humidity);
			period.add(dayTemp);
			period.add(date);
			period.add(main);
			period.add(description);
			
			historicalForecastData.add(period);
			
			
			unixTemp = unixTemp + 86400;
			
		}
		return historicalForecastData;
	}
	
	
	
	
	private void createLogFile() {
		file = new File("data/historicalData" + period + ".txt");
		file.getParentFile().mkdirs();
		if(file.exists())
			file.delete();
		try {
			file.createNewFile();
			fstream = new FileWriter(file, true);
			out = new BufferedWriter(fstream);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void calculateData() throws ParseException, IOException {
		
		createLogFile();
		
		Double ETRain = new Double(0);
		Double deltaSM = new Double(0);

		Double prevETRain = new Double(0);
		Double prevSM = new Double(0);
		
		boolean noValue = false; 
	

		
		Calendar calendar = new GregorianCalendar();
	    calendar.setTime(startDate);
	    
	    SoilMoistureGateway sg = new SoilMoistureGateway(city); //, period, calendar);
	    
	    /*quando scrivo il file mi serve anche il soil moisture al tempo +1 quindi mi conviene già calcolarli tutti ora*/
	    while (calendar.getTime().before(endDate)) {
	    	
	    	System.out.print("Asking for soil moisture data... " + this.getDate(calendar) + " " + this.getTime(calendar));
	    	soilMoistures = sg.getSoilMoistureData(period, calendar, 1);
	    	System.out.println(".. Obtained data from: " + sg.getUrl());
	    	
	    	SoilMoistureListClass historicalSoilMoisturesPeriod = null;
			if (soilMoistures.length != 0) {
				
				historicalSoilMoisturesPeriod = new SoilMoistureListClass(soilMoistures);
				
			} else {
				historicalSoilMoisturesPeriod = new SoilMoistureListClass();
				historicalSoilMoisturesPeriod.add(new SoilMoistureClass(startDate, new Double(-1)));
				
			}
			historicalSoilMoistures.add(historicalSoilMoisturesPeriod);
			
			calendar.add(Calendar.HOUR, period);
			
	    	
	    }
	    
	    calendar.setTime(startDate);
	    int i = 0;
	    
	    //ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(HttpClients.createDefault());
	    
	    
	    RainFallGateway rfg = new RainFallGateway(city); //, period, calendar);
	    HistoricalWeatherGateway wg = new HistoricalWeatherGateway(city); //, period, calendar);
	    
	    while (calendar.getTime().before(endDate)) {
	    	
	    	
	    	
	    	/*I need to add the period because I need the data until that period*/
			calendar.add(Calendar.HOUR, period);
	    	
	    	
			System.out.print("Asking for rainfall data... ");
	    	rainFalls = rfg.getRainFallData(period, calendar);
	    	System.out.println(".. Obtained data from: " + rfg.getUrl());
	    	
	    	Double rainfall = new Double(-1);
			if(rainFalls.length > 0)
				rainfall = calculateHistoricalRain(rainFalls);
			
			calendar.add(Calendar.HOUR, -period);
		
	    	
	    	//historicalData = wg.getWeatherGathering(requestFactory);
			System.out.print("Asking for weather historical data... ");
			historicalData = wg.getWeatherData(period, calendar);
			System.out.println(".. Obtained data from: " + wg.getUrl());
	    	
	    	historicalData.calculateIntervalMinMax();
			historicalData.calculateIntervalHumidity();
			historicalData.calculateIntervalPressure();
			history.add(historicalData);
		
			
			Double min = history.get(i).getIntervalMINInCelsius();
			Double max = history.get(i).getIntervalMAXInCelsius();
			Temperature t = new Temperature(min, max);
			Double windSpeed = history.get(i).getSpeed();
			Double pressure = history.get(i).getIntervalPressure();
			Double humidity = history.get(i).getIntervalHumidity();
	    	
	    	
	   
			
			ReferenceEvapotraspiration refEvap = new ReferenceEvapotraspiration(t, new MiltonKeynes(true, 52.0, 0.0, 114, true, true), new WindSpeed(windSpeed));

			double ETo = refEvap.getETo();
			double smNextPeriod = -1;
			double sm = calculateAverage(historicalSoilMoistures.get(i));
			
			if(i+1 != historicalSoilMoistures.size())
				smNextPeriod = calculateAverage(historicalSoilMoistures.get(i+1));
			
			
			
			//ArrayList xi = new ArrayList<Double>();
			
			if(i==0) {

				out.append("Data" + "," + "ETo" + "," +"Rainfall" + "," + "SM(real)"+ "," + "(Rainfall-ETo)" + "," + "DetaSM" + "," + "WindSpeed" + "," + "minTemp" + "," + "maxTemp" + "," + "pressure" + "," + "humidity" +","+ "y=SM(t+1)");
				out.newLine();
				out.append(getDate(calendar)+ "" + getTime(calendar) + "," + ETo + "," + rainfall + "," + sm  + /*"," + movingAveragesSM.get(i) +*/ "," + ETRain + "," + "NULL" + "," + windSpeed + "," + min + "," + max +"," + pressure +"," + humidity + "," + smNextPeriod);
				out.newLine();
				
				/*
				xi.add(sm);
				xi.add(windSpeed);
				xi.add(min);
				xi.add(max);
				xi.add(pressure);
				xi.add(humidity);
				xi.add(rainfall);
				
				y.add(smNextPeriod);
				//System.out.println(i);
				x.add(xi);
				*/
				
			} else {

				
				//deltaSM = prevSM - movingAveragesSM.get(i);

				deltaSM =  sm - prevSM;

				ETRain = (rainfall - ETo);

				if(sm == -1 || rainfall == -1 || (Double.isNaN(ETo) || Double.isNaN(windSpeed) ||  Double.isNaN(pressure) || Double.isNaN(humidity)) || smNextPeriod == -1) {
					noValue = true;
					
				} else {
					if(noValue){
						noValue = false;
						out.append(getDate(calendar) + getTime(calendar) + "," + ETo + "," + rainfall + "," + sm + "," + ETRain + "," + "NULL" + "," + windSpeed + "," + min + "," + max +"," + pressure +"," + humidity + "," + smNextPeriod );
					} else {
						out.append(getDate(calendar) + getTime(calendar)+ "," + ETo + "," + rainfall + "," + sm + "," + ETRain + "," + deltaSM + "," + windSpeed + "," + min + "," + max +"," + pressure +"," + humidity + ","  + smNextPeriod );
					} 
					out.newLine();
					
					/*
					xi.add(sm);
					xi.add(windSpeed);
					xi.add(min);
					xi.add(max);
					xi.add(pressure);
					xi.add(humidity);
					xi.add(rainfall);
					
					y.add(smNextPeriod);
					
					x.add(xi);
					*/
				}

			}

			
			prevSM = sm;
			
			out.flush();
			
			i++;
			
			
			
			calendar.add(Calendar.HOUR, period);
			
			
			
	    }
	    
	    out.close();
	    
	    
	    
		
		
	}
	
	
	public double[][] getX() {
		
		int row = x.size();
		int col = x.get(0).size();
		
		//System.out.println("row " + row + " col " + col);
		
		double [][] xArray = new double[row][col];
		
		for (int i=0; i< x.size(); i++) {
			
			ArrayList<Double> list = x.get(i);
			
			for(int j=0; j< list.size(); j++) {
				//System.out.println(j);
				xArray[i][j] = (double) list.get(j);
			}
		}
		
		return xArray;
		
	}
	
	
	public double[] getY()  {
		int size = y.size();
		
		double [] yArray = new double[size];
		
	
		for(int j=0; j< y.size(); j++) {
			yArray[j] = (double) y.get(j);
		}
		
		return yArray;
	}
	
	

	
	/*private void run() throws ParseException, IOException {



			RestTemplate restTemplateForecast = new RestTemplate();
			//WeatherForecast forecast = restTemplateForecast.getForObject("http://api.openweathermap.org/data/2.5/forecast/daily?q=Milton%20Keynes,GB&units=metric&cnt=2&APPID=d4bae7baac195853e82a76a8bd489cbf", WeatherForecast.class);
			forecast = restTemplateForecast.getForObject("http://api.openweathermap.org/data/2.5/forecast/daily?q=Milton Keynes,GB&units=metric&cnt=16&APPID=d4bae7baac195853e82a76a8bd489cbf", WeatherForecast.class);

			//WeatherForecast forecast = restTemplate.getForObject("http://api.openweathermap.org/data/2.5/forecast/", WeatherForecast.class);
			//log.info(forecast.toString());
			//initialization(forecast);
			
			
	
		
		//System.out.println("FINE!!");


	}*/
	



	private Double calculateAverage(SoilMoistureListClass sms) throws ParseException {
		
		
		Date limitData = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		Double sum = new Double(0);
		int count = 0;
		Date currentDate;
		String hour = "22:00:00"; 
		
		Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
		calendar.setTime(sms.get(0).getTime());   // assigns calendar to given date 
		calendar.add(Calendar.DATE, -1);	
		
		String data = getDate(calendar);
		//System.out.println("ADTA primo elemento " +data);
		
		limitData = sdf.parse(data+"T"+hour+"Z");
		
		// System.out.println("SIZE "+ sms.size());
		for(int i=0; i< sms.size(); i++) {
			
			currentDate = sms.get(i).getTime();
			
			//Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
			//calendar.setTime(date);   // assigns calendar to given date 
			//System.out.println("data di fine "+limitData.toString() + " " + currentDate.toString());
	
			//limitData = sdf.parse(calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.MONTH) + "-" + calendar.get(Calendar.DAY_OF_MONTH) + "'T'" + hour + "'Z'") ;
			if(currentDate.after(limitData)) { //I keep the value for the average 
				sum += sms.get(i).getValue();
				count++;
			}


		}
		
		//TODO - to fix
		//if the value are all too old, I take the average of all value... 
		if(count == 0)  {
			for(int i=0; i< sms.size(); i++) {
				
				currentDate = sms.get(i).getTime();
				
				//Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
				//calendar.setTime(date);   // assigns calendar to given date 
				sum += sms.get(i).getValue();
				count++;

			}
		}
		
		
		// System.out.println("media: " + sum/count + ", count " + count);
		return sum/count;

		//return new SoilMoistureClass();

	}


	


	private String getDate(Calendar calendar) {
		// TODO Auto-generated method stub
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH)+1; 
		String m;
		String d;
		if(month < 10)
			m = "0"+String.valueOf(month);
		else
			m = String.valueOf(month);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		if(day < 10)
			d = "0"+String.valueOf(day);
		else
			d = String.valueOf(day);
		
		return year +"-"+m+"-"+d;
	}
	
	private String getTime(Calendar calendar)  {
		
		
		String time;
		if(calendar.get(calendar.HOUR_OF_DAY) < 10) {
			time = "T0"+ calendar.get(calendar.HOUR_OF_DAY) +":00:00Z";
		} else {
		
			time = "T"+ calendar.get(calendar.HOUR_OF_DAY) +":00:00Z";
		}
		return time;
	}


	private Double calculateHistoricalRain(RainFallDH[] rf) {
		// TODO Auto-generated method stub
		/*
		 * 1: richiedi per ogni giorno le info sulla pioggia, valori 86 (4 valori per 24 ore)
		 * 2: raggruppa facendo la media i valori di pioggia per ogni ora
		 * 3: somma i valori ottenuti per ogni ora 
		 * 4: risultato: mm caduti al giorno -- mm/day
		 */
		Double mmday = new Double(0);

		Double hour = new Double(0); 

		for(int i=0; i<rf.length; i++) {
			if(i % 4 == 0) { //inizio una nuova ora
				mmday += (hour/4);
				//System.out.println(mmday);
				hour = new Double(0);

			}
			
			hour += rf[i].getValue();
			//System.out.println("PIOGGIA " + rainFalls[i].getValue());


		}


		return mmday;


	}




	/*public void movingMediaSM(int period) {

		int aggPer = period -1; 
		for(int i=0; i<historicalSoilMoistures.size(); i++) {
			Double value = new Double(0);
			//Double nullValue = new Double(-1);
			//System.out.println("________________________________ " + historicalSoilMoistures.get(i).getValue());
			if(historicalSoilMoistures.get(i).getValue() == -1) {
				//System.out.println("CHE CAZZO");
				SoilMoistureClass sm = new SoilMoistureClass(historicalSoilMoistures.get(i).getTime(), movingAveragesSM.get(i-1));
				//System.out.println(historicalSoilMoistures.get(i).getTime() + " " + movingAveragesSM.get(i-1));
				historicalSoilMoistures.set(i, sm);
			}


			if(i<aggPer) {
				value = historicalSoilMoistures.get(i).getValue();
				movingAveragesSM.add(value);
			} else {

				for(int j = i; j >= (i-aggPer); j--) {

					value = value + historicalSoilMoistures.get(j).getValue();

				}

				value = value / period;
				movingAveragesSM.add(value);
			}
		}
		//return value;

	}*/


    
	
	





}