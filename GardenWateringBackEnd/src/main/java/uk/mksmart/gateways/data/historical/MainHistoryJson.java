package uk.mksmart.gateways.data.historical;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MainHistoryJson {
	
	private Double pressure;
	private Double humidity;
	
	private Double temp_min;
	private Double temp_max;
	
	
	public Double getMax() {
		return temp_max;
	}
	
	public Double getMin() {
		return temp_min;
	}
	
	public Double getPressure() {
		return pressure;
	}
	
	
	public Double getHumidity() {
		return humidity;
	}
	
	

}
