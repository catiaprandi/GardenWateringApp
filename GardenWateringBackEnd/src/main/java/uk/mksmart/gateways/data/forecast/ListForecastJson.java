package uk.mksmart.gateways.data.forecast;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)

public class ListForecastJson {
	
	 private Long dt;
	 private TempForecastJson temp;
	 private Double pressure;
	 private Double humidity;
	 private Weather[] weather;
	 private Double speed;
	 private Double deg;
	 private Double clouds;
	 private Double rain;
	 
	 
	 public TempForecastJson getTemp() {
		 return temp;
	 }
	 
	 public Double getSpeed() {
		 return speed;
	 }
	 
	 public Double getRain() {
		 if(rain == null) {
			 rain = new Double(0);
		 }
		 return rain;
	 }
	 
	 
	 public Double getPressure() {
		 return pressure;
	 }
	 
	 public Double getHumidity() {
		 return humidity;
	 }
	 
	 public Weather[] getWeather() {
		 return weather;
	 }
	 
	 public long getDT() {
		 return dt;
	 }
}
