package uk.mksmart.gateways.data.forecast;

import java.util.Date;

import uk.mksmart.gateways.WeatherForecastGateway;
import uk.mksmart.gateways.data.Data;

public class ManageForecastData {

	private WeatherForecastJson forecastData;
	private String city;
	
	
	public ManageForecastData(String city) {
		this.city = city;
	}
	
	public ForecastData getForecastWeatherData(int period)  {
		
		WeatherForecastGateway wfg = new WeatherForecastGateway(this.city);
		forecastData = wfg.getWeatherData(period);
		
		return convertToObj();
		
		
	}
	
	
	private ForecastData convertToObj() {
		
		ListForecastJson[] list = this.forecastData.getList();
		
		ForecastData forecastDataClean = new ForecastData();
		for(int i=0; i < list.length; i++)  {
			ForecastDataPeriod period = new ForecastDataPeriod();
			
			ListForecastJson item = list[i];
			
			
			Data rain = new Data("Rainfall", item.getRain());
			Data sm = new Data("SM(real)", -1);
			Data speed = new Data("WindSpeed", item.getSpeed());
			Data pressure = new Data("pressure", item.getPressure());
			Data humidity = new Data("humidity", item.getHumidity());
			
			TempForecastJson temp = item.getTemp();
			Data minTemp = new Data("minTemp", temp.getMin());
			Data maxTemp = new Data("maxTemp", temp.getMax());
			//to show in the smart calendar (not to use in predict SM)
			Data dayTemp = new Data("dayTemp", temp.getDay());
			Data main = new Data("main", item.getWeather()[0].getMain());
			Data description = new Data("description", item.getWeather()[0].getDescription() );
			
			long timeStamp = item.getDT();
			Date time = new Date(timeStamp*1000);
			//System.out.println(time);
			
			Data date = new Data("date", time.toString());
			
			
			
			//Data day = new Data();
			
			period.add(rain);
			period.add(sm);
			period.add(speed);
			period.add(minTemp);
			period.add(maxTemp);
			period.add(pressure);
			period.add(humidity);
			period.add(dayTemp);
			period.add(date);
			period.add(main);
			period.add(description);
			
			forecastDataClean.add(period);
			
		}
		
		return forecastDataClean;
		
	}
	
}
