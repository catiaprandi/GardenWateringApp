package uk.mksmart.gateways.data.forecast;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {
	
	private Integer id;
	private String main;
	private String description;
	private String icon;
	
	
	public String getMain() {
		return main;
	}
	
	public String getDescription() {
		return description;
	}

}
