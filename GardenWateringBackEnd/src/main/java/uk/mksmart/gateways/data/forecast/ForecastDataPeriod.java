package uk.mksmart.gateways.data.forecast;

import java.util.ArrayList;
import java.util.List;

import uk.mksmart.gateways.data.Data;

public class ForecastDataPeriod {

	
	private List<Data> list; 
	
	
	public ForecastDataPeriod() {
		list = new ArrayList<Data>();
	}
	
	public void add(Data data) {
		list.add(data);
	}
	
	public Data get(int i)  {
		return list.get(i);
	}
	
	public int size() {
		return list.size();
	}
	
	public void set(int i, Data data) {
		list.set(i, data);
	}
}
