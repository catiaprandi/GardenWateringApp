package uk.mksmart.gateways.data.historical;

import java.util.ArrayList;
import java.util.List;

import uk.mksmart.gateways.data.Data;

public class HistoricalDataPeriod {

		private String dateTime;
		private List<Data> list; 
		
		
		public HistoricalDataPeriod(String datetime) {
			this.dateTime = datetime;
			list = new ArrayList<Data>();
		}
		
		public void add(Data data) {
			list.add(data);
			
		}
		
		public Data get(int i) {
			return list.get(i);
		}
		
		public double[] getXValues(){
			double[] values = new double[list.size()-1]; //the last value is Y
			
			for(int i=0; i<list.size()-1; i++) { //the last value is Y
				values[i] = (Double)list.get(i).getValue();
			}
			return values;
		}
		
		public double getYValue() {
			return (Double)list.get(list.size()-1).getValue();
		}
		
		public int getXSize() {
			return list.size()-1;
		}
		
		public int size() {
			return list.size();
		}
		
		//TODO: remove the name from the code!!!
		public double getSM() {
			for(int i=0; i<list.size()-1; i++) { //the last value is Y
				
				if(list.get(i).getName().equals("SM(real)")) {
					
					return (Double)list.get(i).getValue();
				}
			}
			return -1;
		}
		
		public void setSM(double value) {
			for(int i=0; i<list.size()-1; i++) { //the last value is Y
				if(list.get(i).getName().equals("SM(real)")) {
					list.get(i).setValue(value);
				}
			}
			
		}
		
		//TODO: remove the name from the code!!!
		public double getRainFall() {
			for(int i=0; i<list.size()-1; i++) { //the last value is Y
				if(list.get(i).getName().equals("SM(real)")) {
					return (Double)list.get(i).getValue();
				}
			}
			return -1;
		}
}
