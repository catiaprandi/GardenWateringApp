package uk.mksmart.gateways.data;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SoilMoistureOld {

	private String time;
	private String value;
	
	
	
	
}
