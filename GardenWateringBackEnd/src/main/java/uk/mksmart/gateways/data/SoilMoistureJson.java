package uk.mksmart.gateways.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



@Data
@JsonIgnoreProperties(ignoreUnknown = true)


public class SoilMoistureJson {
	
	private String time;
	private String value;
	private Double wkt;
	
	private Double numValue = new Double(-1);
	
	
	public String getValue() {
		return value;
	}
	
	public Double getNumValue() {
		return numValue;
	}
	
	/*public String getTimeToString() {
		return time;
		
	}*/
	
	
	/**
	 * Return the date in the format: yyyy-MM-dd'T'HH:mm:ss'Z'
	 * @return
	 */
	public Date getDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z");
		Date date = new Date();
		try {
			date = sdf.parse(time);
			//System.out.println("TIME " + time);
			SimpleDateFormat sdfNew = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			String newFormart = sdfNew.format(date);
			
			date = sdfNew.parse(newFormart);
			
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return date;
	}
	
	
	public Double getWkt() {
		return wkt;
	}

	

	

	
}
