package uk.mksmart.gateways.data;

import java.util.ArrayList;
import java.util.List;

public class SoilMoistureListClass {
	
	private List<SoilMoistureClass> hsm = null;
	
	
	public SoilMoistureListClass() {
		hsm = new ArrayList<SoilMoistureClass>();
	}

	public SoilMoistureListClass(SoilMoistureJson[] sms) {
		
		hsm = new ArrayList<SoilMoistureClass>();
		
		for (int i = 0; i < sms.length; i++) {

			String value = sms[i].getValue();

			if (value.length() > 4) {

				// first two characters are useless, then use the 3,4 - 7,8 -
				// 13,14 and 15,16
				// System.out.println("VALUE " +value);
				for (int j = 2; (j < value.length() && j < 21); j = j + 6) {

					int decimal = Integer.parseInt(value.substring(j, j + 2),
							16);
					double meausure = (double) decimal / 2.55;
					// System.out.println("VALEU +++++ " + j + " "+
					// value.substring(j, j+2) + " " + meausure);

					hsm.add(new SoilMoistureClass(sms[i].getDate(), new Double(meausure)));
					// System.out.println(j);
				}

			} else {
				hsm.add(new SoilMoistureClass(sms[i].getDate(), Double.valueOf(value)));
			}

		}
		
	}

	public void add(SoilMoistureClass sm) {
		hsm.add(sm);
	}

	public SoilMoistureClass get(int i) {
		return hsm.get(i);
	}

	public int size() {
		return hsm.size();
	}
	
	public double getLastValue() {
		return hsm.get(0).getValue();
	}
	
	
}
