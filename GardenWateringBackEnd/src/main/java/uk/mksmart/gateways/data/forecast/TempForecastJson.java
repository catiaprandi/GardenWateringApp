package uk.mksmart.gateways.data.forecast;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)

public class TempForecastJson {
	
	private Double day;
	private Double min;
	private Double max;
	private Double night;
	private Double eve;
	private Double morn;
	
	
	public Double getMin() {
		return min;
	}
	
	
	
	public Double getMax() {
		return max;
	}
	
	public Double getDay() {
		return day;
	}
	
	

}
