package uk.mksmart.gateways.data.forecast;

import java.util.ArrayList;
import java.util.List;

public class ForecastData {
	
	private List<ForecastDataPeriod> list;
	
	public ForecastData() {
		list = new ArrayList<ForecastDataPeriod>();
	}
	
	public void add(ForecastDataPeriod period) {
		list.add(period);
	}
	
	public ForecastDataPeriod get(int i) {
		return list.get(i);
	}
	
	public int size() {
		return list.size();
	}

}
