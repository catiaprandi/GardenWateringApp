package uk.mksmart.gateways.data;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RainFallDH {
	
	private String time;
	private Double value;
	private String wkt;
	
	private Double mmDay;
	
	
	public Double getValue() {
		return value;
	}
	
}
