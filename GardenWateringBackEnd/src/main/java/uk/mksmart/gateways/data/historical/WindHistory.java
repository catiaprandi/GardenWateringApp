package uk.mksmart.gateways.data.historical;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)

public class WindHistory {
	
	private Double speed;

	
	
	public Double getSpeed() {
		return speed;
	}
}
