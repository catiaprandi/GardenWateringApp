package uk.mksmart.gateways.data;

public class Data {
	
	private String name;
	private Object value;
	
	public Data(String name, Object value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
	
	

}
