package uk.mksmart.gateways.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import uk.mksmart.gateways.data.historical.HistoricalData;
import uk.mksmart.gateways.data.historical.HistoricalDataPeriod;
import uk.mksmart.main.GardenWateringApplication;

public class DataFile {
	
	private File file;
	private BufferedReader reader;
	private HistoricalData history;
	private boolean ET = false;
	
	
	
	public DataFile(String fileName) {
		
		file = new File(fileName);
		//convertDataInObj();
		
	}
	
	
	public DataFile(String fileName, boolean ET) {
		
		file = new File(fileName);
		this.ET = ET;
		//convertDataInObj();
		
	}
	
	
	public HistoricalData convertDataInObj() {
		//leggo il file e salvo i valori in oggetti
		try {
		    reader = new BufferedReader(new FileReader(file));
		    String line = null;
		    history = new HistoricalData();
		    HistoricalDataPeriod histPeriod; 
		    String descr = (String) reader.readLine(); //first line is for description
		    String descrs[] = descr.split(",");
		    while ((line = reader.readLine()) != null) {
		    	String values[] = line.split(",");
		    	histPeriod = new HistoricalDataPeriod(values[0]);
		    	
		    	for(int j=1; j<values.length; j++ ) { //0 is the datetime
		    		if(ET == false) {
		    			if(j == 0 || j == 1 || j == 4  || j == 5 || j == 7 || j == 8 || j == 10) {
			    			
			    			//continue;
			    			
				    	} else {
				    		
				    		Data data = new Data(descrs[j], Double.parseDouble(values[j]));
				    		//System.out.println(descrs[j] + " " + Double.parseDouble(values[j]));
				    		histPeriod.add(data);
				    		
				    	}
		    		} else  {//considering also ET value and ET-Rainfall value
		    			if(j == 0 || j == 5) {
			    			
			    			continue;
			    			
				    	} else {
				    		//System.out.println("QUi2 "+ j);
				    		Data data = new Data(descrs[j], Double.parseDouble(values[j]));
				    		//System.out.println(descrs[j] + " " + Double.parseDouble(values[j]));
				    		histPeriod.add(data);
				    		
				    	}
		    		}
		    		
		    	}
		
		    	history.add(histPeriod);
		    	//history.setDescription(descrs);
		    	
		    }
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		} finally {
		    try {
		        if (reader != null) {
		            reader.close();
		        }
		    } catch (IOException e) {
		    }
		}
		
		
		
		/*for(int i=0; i < history.getDescription().length; i++) {
			System.out.print(history.getDescription()[i]);
			
		}*/
		
		
		
		return history;
	}
	
	/*public double[][] getXValues() {
		
		return history.getXValues();
	}
	
	public double[] getYValue() {
		return history.getYValue();
	}*/
	
	//public getX
	
	//public getY

}
