package uk.mksmart.gateways.data.historical;

import java.util.ArrayList;
import java.util.List;

public class HistoricalData {
	
	private List<HistoricalDataPeriod> list;
	//private String[] descrs;
	
	public HistoricalData() {
		list = new ArrayList<HistoricalDataPeriod>();
	}
	
	public void add(HistoricalDataPeriod hdp) {
		list.add(hdp);
	}
	
	public HistoricalDataPeriod get(int i) {
		return list.get(i);
	}
	
	/*public void setDescription(String[] d) {
		this.descrs = d;
	}
	
	public String[] getDescription()  {
		return this.descrs;
	}*/
	
	public double[][] getXValues() {
		
	
		double[][] values = new double[list.size()][]; 
		double[] valuesForPeriod;
		
		for(int i=0; i<list.size(); i++) {
			valuesForPeriod = list.get(i).getXValues();
			
			values[i] = valuesForPeriod;
		}
		
		//System.out.println("Lunghezza in historical data  " + );
		
		return values;
	}
	
	public double[] getYValue() {
		double[] values = new double[list.size()]; 
		for(int i=0; i<list.size(); i++) {
			double valueForPeriod = list.get(i).getYValue();
			values[i] = valueForPeriod;
		}
		return values;
	}
	
	
	

	public void cleanData() {
		
		
		//TODO: valutare con diversi valori
		double confindenceValue = 5;
		
		double rain = 0;
		double sm = 0;
		double smPre = 0;
		
		for(int i=1; i<list.size()-1; i++) {
			
			smPre = list.get(i-1).getSM();
			sm = list.get(i).getSM();
			//se il nuovo valore è maggiore di 	quello precedente di almeno 5
			if((sm - smPre) > confindenceValue) {
				//controllo se è piovuto!
				rain = list.get(i-1).getRainFall();
				if(rain > 0) {
					if(i != list.size()-2) {
				
						//se è piovuto controllo se è avvenuto un rimbalzo da pioggia, cioè ho solo un valore molto alto poi torna stabile
						double smNext1 = list.get(i+1).getSM();
						double smNext2 = list.get(i+2).getSM();
						//TODO per 3 ore conviene controllare più valori!!! non solo due!
						//Per 24 ore due valori già significa 2 giorni dopo
						if(Math.abs(smNext1 - smNext2) < confindenceValue) {
							//SI, rimbalzo da pioggia
							//assegno ad sm corrente la media
							sm = (smPre + smNext1)/2;
							//System.out.println("vecchio valore " + list.get(i).getSM());
							list.get(i).setSM(sm);
							//System.out.println("nuovo valore " + list.get(i).getSM());
							
						}
					}
					
				} else  {
					//pazzia o annaffiatura!!!!
					double smNext1 = list.get(i+1).getSM();
					if(Math.abs(smNext1 - smPre) < confindenceValue ) {
						//pazzia
						sm = Math.abs(smPre + smNext1)/2;
						//System.out.println("vecchio valore " + list.get(i).getSM());
						list.get(i).setSM(sm);
						//System.out.println("nuovo valore " + list.get(i).getSM());
					} else if(Math.abs(sm - smNext1) < confindenceValue) {
						
						//annaffiatura -> resta alto
						System.out.println("annaffiatura");
						sm = (smNext1 + sm)/2;
						list.get(i).setSM(sm);
						
					}
					
				}
				
				
			
			
			}
		}
		
		
		
		
	}
	
	
	
	

}
