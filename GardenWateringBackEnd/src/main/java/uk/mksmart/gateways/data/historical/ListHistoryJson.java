package uk.mksmart.gateways.data.historical;

import uk.mksmart.gateways.data.Rain;
import uk.mksmart.gateways.data.forecast.Weather;
import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)

public class ListHistoryJson {
	
	private MainHistoryJson main;
	private WindHistory wind;
	private Rain rain;
	private Weather[] weather;
	private long dt;
	
	
	public MainHistoryJson getMain() {
		return main;
	}
	
	public WindHistory getWind() {
		return wind;
	}
	
	
	public long getdt()  {
		return dt;
	}
	
	public Rain getRain() {
		return rain;
	}
	
	public String getWeatherDescription() {
	
		return weather[0].getDescription();
	}
	
	
	
	

}
