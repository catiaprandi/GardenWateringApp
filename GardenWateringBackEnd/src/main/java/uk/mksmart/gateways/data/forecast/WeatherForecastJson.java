package uk.mksmart.gateways.data.forecast;


import uk.mksmart.gateways.data.CityInformation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)

public class WeatherForecastJson {

	    //private String type;
	    //private Value value;
	    private CityInformation city;
	    private Integer cod;
	    private String message;
	    private Integer cnt;
	    private ListForecastJson[] list;
	    
	    
	    public CityInformation getCity() {
	    	return city;
	    }
	    
	    public ListForecastJson[] getList() {
	    	return list;
	    }
	    
	   

	
}
