package uk.mksmart.gateways.data.historical;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoricalWeatherJson {
	
	private String message;
	private String cod;
	private Long city_id;
	private Double calctime;
	private Integer cnt; //deve essere 24
	private ListHistoryJson[] list; //temp min and temp max, pressure and humidity
	
	private Double intervalMAX;
	private Double intervalMIN;
	
	private Double intervalPressure;
	private Double intervalHumidity;
	
	
	public void calculateIntervalPressure() {

		// Double[] values = new Double[2];
		Double tot = new Double(0);
		
		int i;
		for (i = 0; i < list.length; i++) {

			Double press = list[i].getMain().getPressure();
			tot += press;
			

		}

		this.intervalPressure = tot/i;

		// return values;

	}
	
	
	
	public void calculateIntervalHumidity() {

		// Double[] values = new Double[2];
		Double tot = new Double(0);
		
		int i;
		for (i = 0; i < list.length; i++) {

			Double hum = list[i].getMain().getHumidity();
			tot += hum;

		}

		this.intervalHumidity = tot/i;

		// return values;

	}
	

	public void calculateIntervalMinMax() {

		// Double[] values = new Double[2];
		Double min = new Double(0);
		Double max = new Double(0);
		
		for (int i = 0; i < list.length; i++) {

			Double minTemp = list[i].getMain().getMin();
			Double maxTemp = list[i].getMain().getMax();

			// System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%% " +minTemp + " " +
			// maxTemp);
			if (i != 0) {
				if (minTemp < min)
					min = minTemp;

				if (maxTemp > max)
					max = maxTemp;
			} else {
				min = minTemp;
				max = maxTemp;
			}

		}

		this.intervalMAX = max;
		this.intervalMIN = min;
		//values[0] = min;
		//values[1] = max;
		
		//return values;
		
	}
	
	
	public Double getIntervalMAXInCelsius() {
		return this.intervalMAX -273.16;
		
	}
	public Double getIntervalMINInCelsius() {
		return this.intervalMIN -273.16;
		
	}
	
	
	public Double getSpeed() {
		//TODO.. media??
		Double totSpeed = new Double(0);
		for(int i=0; i<list.length; i++ ) {
			totSpeed += list[i].getWind().getSpeed();
		}
		
		return (totSpeed/list.length);
		
	}
	
	public Double getIntervalHumidity() {
		return this.intervalHumidity;
	}
	
	public Double getIntervalPressure() {
		return this.intervalPressure;
	}
	
	/*public Double getRain() {
		//TODO
		
		return new Double(0);
		
	}*/
	
	public String getDescription() {
		int[] count = new int[26];
		String[] value = new String[26];
		//System.out.println("LUNGHEZZA ********" + list.length);
		
		for(int i=0; i<list.length; i++ ) {
			String desc = list[i].getWeatherDescription();
			boolean find = false;
			for(int j=0; j<24; j++) {
				if(desc == value[j]) {
					count[j] ++;
					find = true;		
				}	
				
			}
			
			if(find == false) {
				count[i] = 1;
				value[i] = desc;
			}
			
		}
		
		int max = 0;
		int index = 0;
		for(int i =0; i< 24; i++) {
			int temp = count[i];
			if(temp > max) {
				max = temp;
				index = i;
			}
		}
		
		System.out.println(value[index]);
		return value[index];
		
	}
	
	public long getDT() {
		return list[0].getdt();
	}
	

}
