package uk.mksmart.gateways;

import java.util.Calendar;
import java.util.Date;

import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.apache.http.impl.client.HttpClients;

import uk.mksmart.gateways.data.historical.HistoricalWeatherJson;

public class HistoricalWeatherGateway {

	//this.historicalData = historicalTemplate.getForObject("http://api.openweathermap.org/data/2.5/history/city?q=Milton Keynes,GB&cnt="+ period +"&start="+longTime+"&APPID=d4bae7baac195853e82a76a8bd489cbf", HistoricalDataJson.class);
	//log.info(historicalData.toString());
	
	private final String SOURCE = "http://api.openweathermap.org/data/2.5/history/city";
	private final String APPID = "d4bae7baac195853e82a76a8bd489cbf";
	
	private String city;
	private int period;
	private String url;
	private long unixTime;
	
	private RestTemplate historicalTemplate;
	
	//private static RestTemplate historicalTemplate;
	
	public HistoricalWeatherGateway(String city) {
		this.city = city;
		
		historicalTemplate = new RestTemplate();
		
	}
	
	
	/**
	 * TODO
	 * Creato SOLO per demo!!!!
	 * @param period
	 * @param unixTime
	 * @return
	 */
	public HistoricalWeatherJson getWeatherDataForForecast(int period, long unixTime) {
		
		String url = SOURCE + "?q=" + city +"&cnt=" + period + "&start=" + unixTime + "&APPID=" + APPID; 
		System.out.println("****************************************" + url);
		HistoricalWeatherJson historicalData  = historicalTemplate.getForObject(url, HistoricalWeatherJson.class);
		return historicalData;
	
	}
		
	
	
	public HistoricalWeatherJson getWeatherData(int period, Calendar calendar) {
		
		this.period = period;
		
		Date result = calendar.getTime();
        this.unixTime = new Long(result.getTime()/1000);
        System.out.println("****************************************" + url);
		this.url = SOURCE + "?q=" + city +"&cnt=" + period + "&start=" + unixTime + "&APPID=" + APPID; 
		
		
		/*
		 * 
		 */
		//ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(HttpClients.createDefault());

		//RestTemplate restTemplate = new RestTemplate();
		
		
		HistoricalWeatherJson historicalData  = historicalTemplate.getForObject(url, HistoricalWeatherJson.class);
		return historicalData;
	}
	
	public String getUrl() {
		return url;
	}

	
	
	
	
	
}