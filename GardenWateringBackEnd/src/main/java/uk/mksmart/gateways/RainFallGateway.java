package uk.mksmart.gateways;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import uk.mksmart.gateways.data.RainFallDH;

public class RainFallGateway {
	
	/**
	 * Number of items in each request, 4 items for hour (each 15 minutes)
	 */
	private final int N_ITEMS = 4;
	
	
	private final String SOURCE = "https://api.beta.mksmart.org/sensors/feeds/7e9c1cf2-8b98-41c3-bbf9-eefe184c4a3b/datastreams/3/datapoints";
	private final String CRED = "6227bc02-99a5-4745-8597-8a4fe60da444:";
	
	/**
	 * Complete URL to the https request
	 * Example: https://api.beta.mksmart.org/sensors/feeds/7e9c1cf2-8b98-41c3-bbf9-eefe184c4a3b/datastreams/3/datapoints?limit=96&end=2015-07-04T02:00:00Z
	 */
	private String url;
	private int limit;
	private String datatime;
	private int period;
	private Calendar calendar;
	
	private RestTemplate templateRainFall;
	private HttpEntity<String> requestRF;
	
	

	public RainFallGateway(String city) {
		
		templateRainFall = new RestTemplate();
		byte[] plainCredsBytesRF = CRED.getBytes();
		byte[] base64CredsBytesRF = Base64.encodeBase64(plainCredsBytesRF);
		String base64CredsRF = new String(base64CredsBytesRF);
		HttpHeaders headersRF = new HttpHeaders();
		headersRF.add("Authorization", "Basic " + base64CredsRF);
		requestRF = new HttpEntity<String>(headersRF);
				
				
	}
	
	private String getTime()  {

		
		String time;
		if(this.calendar.get(Calendar.HOUR_OF_DAY) < 10) {
			time = "T0"+ calendar.get(Calendar.HOUR_OF_DAY) +":00:00Z";
		} else {
		
			time = "T"+ calendar.get(Calendar.HOUR_OF_DAY) +":00:00Z";
		}
		System.out.println(time);
		return time;
	}
	
	private String getDate() {
		// TODO Auto-generated method stub
		int year = this.calendar.get(Calendar.YEAR);
		int month = this.calendar.get(Calendar.MONTH)+1; 
		String m;
		String d;
		if(month < 10)
			m = "0"+String.valueOf(month);
		else
			m = String.valueOf(month);
		int day = this.calendar.get(Calendar.DAY_OF_MONTH);
		if(day < 10)
			d = "0"+String.valueOf(day);
		else
			d = String.valueOf(day);
		
		System.out.println(year +"-"+m+"-"+d);
		return year +"-"+m+"-"+d;
	}
	
	public  RainFallDH[] getRainFallData(int period, Calendar calendar) {
		
		this.calendar = calendar;
		this.limit = period * N_ITEMS;
		String time = getTime();
		String date = getDate();
		this.datatime = date + "" + time;
		
		this.url = SOURCE + "?end="+ datatime + "&limit=" + limit;
		
		
		
		ResponseEntity<RainFallDH[]> responseRain = templateRainFall.exchange(url, HttpMethod.GET, requestRF, RainFallDH[].class);

		RainFallDH[] rainFalls = responseRain.getBody();
		
		return rainFalls;
	}
	
	public String getUrl() {
		return url;
	}
	
	
}
