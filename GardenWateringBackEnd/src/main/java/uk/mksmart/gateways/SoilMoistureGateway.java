package uk.mksmart.gateways;

import java.util.Calendar;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import uk.mksmart.gateways.data.SoilMoistureJson;

public class SoilMoistureGateway {
	
	private final String SOURCE = "https://api.beta.mksmart.org/sensors/feeds/";

	private String url;
	
	private String plainCredsSMECD6 = "5195b528-bac7-491b-b49d-493fe9eaa7c6";
	private String ECD6 = "a7ceaace-bc9b-4306-b9c4-0e1e59e63208/datastreams/60630/";
	
	private String plainCredsSM1ED15 = "da124337-7c75-4e2a-8c48-dcf70aeb4f43:";
	private String _1ED15 = "c37f5cbe-987f-415f-80f7-cd5911da92b1/datastreams/0/";
	
	//private String date;
	//private String time = "T00:01:00Z";
	private int limit; // = 1;
	
	private RestTemplate templateSoilMoisture;
	private HttpEntity<String> requestSM;
	
	
	public SoilMoistureGateway(String city) {
		
		templateSoilMoisture = new RestTemplate();
		byte[] plainCredsBytesSM = plainCredsSMECD6.getBytes();
		
		byte[] base64CredsBytesSM = Base64.encodeBase64(plainCredsBytesSM);
		String base64CredsSM = new String(base64CredsBytesSM);
		HttpHeaders headersSM = new HttpHeaders();
		headersSM.add("Authorization", "Basic " + base64CredsSM);
		requestSM = new HttpEntity<String>(headersSM);
		
		
	}
	
	private String getDate(Calendar calendar) {
		// TODO Auto-generated method stub
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH)+1; 
		String m;
		String d;
		if(month < 10)
			m = "0"+String.valueOf(month);
		else
			m = String.valueOf(month);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		if(day < 10)
			d = "0"+String.valueOf(day);
		else
			d = String.valueOf(day);
		
		return year +"-"+m+"-"+d;
	}
	
	
	private String getTime(Calendar calendar)  {

		
		String time;
		if(calendar.get(Calendar.HOUR_OF_DAY) < 10) {
			time = "T0"+ calendar.get(Calendar.HOUR_OF_DAY) +":00:00Z";
		} else {
		
			time = "T"+ calendar.get(Calendar.HOUR_OF_DAY) +":00:00Z";
		}
		return time;
	}
	
	
	
	public SoilMoistureJson[] getSoilMoistureData(int period, Calendar calendar, int limit) {
		
		this.limit = limit;
		this.url = SOURCE + ECD6 + "datapoints?"+ "end=" + getDate(calendar) + getTime(calendar) + "&" + "limit=" + limit;
		
		
		ResponseEntity<SoilMoistureJson[]> response = templateSoilMoisture.exchange(url, HttpMethod.GET, requestSM, SoilMoistureJson[].class);
		
		SoilMoistureJson[] soilMoistures = response.getBody();
		
		return soilMoistures;

	}
	
	
public SoilMoistureJson[] getCurrentSoilMoistureData() {
		
		
		this.url = SOURCE + ECD6 + "datapoints?"+ "limit=1";
		
		
		ResponseEntity<SoilMoistureJson[]> response = templateSoilMoisture.exchange(url, HttpMethod.GET, requestSM, SoilMoistureJson[].class);
		
		SoilMoistureJson[] soilMoistures = response.getBody();
		
		return soilMoistures;

	}
	
	
	public String getUrl() {
		return url;
	}
	
	

}
