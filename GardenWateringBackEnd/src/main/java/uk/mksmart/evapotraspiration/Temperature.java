package uk.mksmart.evapotraspiration;


public class Temperature {

	
	/**
	 * max temperature [°C]
	 */
	private Double Tmax;
	
	/**
	 * min temperature [°C]
	 */
	private Double Tmin;
	
	/**
	 * Defined if the climate stations used to surrounded by a well watered crop (true) or not (false)
	 * Default value: false
	 */
	private boolean reference = false;
	
	/** Dev point temperature [°C]
	 * 
	 */
	private static Double Tdew; 
	
	
	/**
	 * 
	 */
	private Double delta_T;
	
	/**
	 * ko is:
	 * 	2 °C if the non-reference station is not compared to a reference station (default value)
	 * 	0 °C otherwise
	 */
	private static Double Ko = 2.0;
	
	
	
	
	/**
	 * Class constructor (default: not-reference climate station and not Tdew available)
	 * Default constructor
	 * @param Tmax
	 * @param Tmin
	 */
	public Temperature(Double Tmin , Double Tmax) {
		this.Tmax = Tmax;
		this.Tmin = Tmin;

	}
	
	
	/**
	 * Class constructor
	 * @param reference - indicated that the climate stations used to surrounded by a well watered crop
	 * @param Tmax
	 * @param Tmin
	 */
	public Temperature(boolean reference, Double Tmax, Double Tmin) {
		this.Tmax = Tmax;
		this.Tmin = Tmin;
		this.reference = reference;
		
	}
	
	/**
	 * Class constructor (default: not-reference climate station) with Tdew available
	 * @param Tmax
	 * @param Tmin
	 */
	public Temperature(Double Tmax, Double Tmin, Double Tdew) {
		this.Tmax = Tmax;
		this.Tmin = Tmin;
		this.delta_T = Tmin - Tdew;
	}
	
	
	
	
	public Double getT() {
		double T = 0;
		if(!reference && Tdew != null) //if we have reference temperature or Tdev not defined
			T = getNotRefT();
		else
			T = (Tmax + Tmin) / 2;
		return T;
	}


	
	/**
	 * Calculate the mean of the maximum and the minimum recorded temperatures with adjust temperatures of non-reference climate station
	 * where Tdev is available
	 * @return T
	 */
	private Double getNotRefT() {
		// TODO Auto-generated method stub
		double Tmax_cor = Tmax - ((delta_T - Ko)/2);
		double Tmin_cor = Tmin - ((delta_T - Ko)/2);

		return (Tmax_cor + Tmin_cor) / 2;
	}
	
	
	/**
	 * 
	 * @return Tmin
	 */
	public Double getTmin() {
		return Tmin;
	}
	
	
	/**
	 * return Tmax
	 * @return
	 */
	public Double getTmax() {
		return Tmax;
	}
	
	
	public Double getKo() {
		return Ko;
	}
	
	
	public Double getTdew() {
		return Tdew;
	}
	
}
