package uk.mksmart.evapotraspiration;


public class VaporPressureDeficit {
	
	
	private Temperature temp;
	
	/**
	 * Class constructor for vapour pressure deficit (kPa), VPD
	 * VDP is the difference between the saturation (e_s) and actual vapour pressure (e_a) for a given time period)
	 */
	public VaporPressureDeficit(Temperature temp) {
		this.temp = temp;
		
	}
	
	
	/**
	 * Calculate e_s, the saturation vapour pressure [kPa]
	 * @return e_s 
	 */
	public Double getE_s() {
		
		return (this.getE_Tmax() + this.getE_Tmin())/2;
	}
	
	public Double getE_Tmin() {
		return 0.6108 * Math.exp((17.27 * temp.getTmin())/ (temp.getTmin() + 237.3));
	}
	
	public Double getE_Tmax() {
		return 0.6108 * Math.exp((17.27 * temp.getTmin())/ (temp.getTmin() + 237.3));
	}
	
	
	/**
	 * Calculare e_a, the actual vapour pressure [kPa]
	 * It can be calculated in three different way:
	 * 1) where Tdew is available
	 * 2) where RH is available (kPa)
	 * 3) where only Tmin is available
	 * 
	 * @return e_a
	 */
	public Double getE_a() {
		Double e_a;
		if(temp.getTdew() != null)
			e_a = 0.6108 * Math.exp((17.27 * temp.getTdew())/(temp.getTdew() + 237.3));
		else if (RelativeHumidity.getRH() != null)
			e_a = RelativeHumidity.getRH()/100 * ((this.getE_Tmax() + this.getE_Tmin()) / 2);
		else
			e_a = 0.6108 * Math.exp((17.27 * (temp.getTmin() - temp.getKo()))/((temp.getTmin() - temp.getKo() + 237.3)));
		
		return e_a;
	}
	
}
