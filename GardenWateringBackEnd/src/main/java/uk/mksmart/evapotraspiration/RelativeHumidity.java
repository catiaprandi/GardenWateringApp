package uk.mksmart.evapotraspiration;



public class RelativeHumidity {
	
	private static Double RH;
	
	/**
	 * if the RH value is not evailable it is possible to use the average worldwide value: 2 m/s
	 */
	private final Double AVERAGE_RH = 2.0;

	
	/**
	 * Class Constructor of Relative Humidity 
	 * 
	 * @param RH
	 */
	public RelativeHumidity(Double RH) {
		if(RH == null)
			this.RH = AVERAGE_RH;
		else
			this.RH = RH;
		
	}
	
	public static Double getRH() {
		return RH;
	}
	
	
}
