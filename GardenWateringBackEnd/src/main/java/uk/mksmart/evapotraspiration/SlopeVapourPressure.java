package uk.mksmart.evapotraspiration;


public class SlopeVapourPressure {
	
	
	private Temperature temp;
	
	
	/**
	 * Slope vapour pressure curve, delta, kPA °C-1
	 * @param temp
	 */
	public SlopeVapourPressure(Temperature temp) {
		
		this.temp = temp;
	}
	
	public Double getDelta() {
		
		//TODO
		//return 4098 * (0.6108 * Math.exp(17.27 * temp.getT() / Math.pow((temp.getT() + 237.13), 2)));
		return 101.55;
	}
}
