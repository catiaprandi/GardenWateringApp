package uk.mksmart.evapotraspiration;



import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;



public class NetRadiation {
	
	private Double alfa = 0.23;
	
	
	private Double sunshine;
	
	private Temperature temp;
	
	private City city;
	
	
	/**
	 * Class constructor for NetRadiation if sunshine hours is available
	 * 
	 * @param sunshine
	 */
	public NetRadiation(Double sunshine, City city)  {
		this.sunshine = sunshine;
		this.city = city;
		
	}
	
	/**
	 * Class construction for NetRadiation if sunshine hours is NOT available
	 * @param Tmin
	 * @param Tmax
	 */
	public NetRadiation(Temperature temp, City city)  {
		this.temp = temp;
		this.city = city;
		
	}
	
	
	/**
	 * Calculate the net solar radiation value (Rns)
	 * 
	 * @return Rns
	 */
	private Double getRns(Double Ra) {
		return (1 - alfa) * getRs(Ra);
	}
	
	
	//TODO
		//sistema sta roba!!!
		//rimane null defaultRA!!!
	/**
	 * public Double getRn() {
	 
		
		return getRns() - getRnl();
	} **/
	
	public Double getRn(Double Ra, Double Rso, Double e_a) {
		
		return getRns(Ra) - getRnl(Ra, Rso, e_a);
	}

	private Double getRnl(Double Ra, Double Rso, Double e_a) {
		// TODO Auto-generated method stub
		return  4.903 * 0.000000001 * (( Math.pow((temp.getTmax() + 273.16), 4) + Math.pow((temp.getTmin() + 273.16), 4) ) 
				/ 2 ) * (0.34 - 0.14 * Math.sqrt(e_a)) * (1.35 * getRs(Ra) / Rso - 0.35);
	}
	
	private Double getRs(Double Ra) {
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int month = cal.get(Calendar.MONTH);
		Double Rs;
		if(sunshine != null)  { //sunshine hours available
			
			Rs = (0.25 + 0.50 * sunshine / city.getN().get(month)) * city.getDefaultRa().get(month);
		} else { //sunshine hours not available
			//if(city.getDefaultRa() == null)
				// System.out.println("cacchio");
			//System.out.println(city.getDefaultRa().get(month));
			
			Rs = city.getKrs() * Math.sqrt(temp.getTmax() - temp.getTmin()) * Ra; 
		}
		return Rs;
	}
	
}
