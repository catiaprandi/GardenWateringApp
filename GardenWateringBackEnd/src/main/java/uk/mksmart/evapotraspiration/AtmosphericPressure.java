package uk.mksmart.evapotraspiration;



public class AtmosphericPressure {
	
	
	private City city;


	public AtmosphericPressure(City city) {
		this.city = city;
	}
	
	
	public Double getP() {
		return 101.3 * Math.pow(((293 - 0.0065  * city.getSeaElevation()) / 293), 5.26);
	}
	
	
	
}
