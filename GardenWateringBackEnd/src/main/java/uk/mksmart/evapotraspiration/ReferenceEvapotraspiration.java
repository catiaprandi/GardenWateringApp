package uk.mksmart.evapotraspiration;

import java.util.Calendar;


public class ReferenceEvapotraspiration {
	
	
	/**
	 * Class to calculate the ReferenceEvapotraspiration
	 */
	
	
	/**
	 * solar constant, MJm-2min-1
	 */
	private final double Gsc = 0.082;
	
	private Temperature temp;
	
	private City city;
	
	private NetRadiation radiation; 
	
	private VaporPressureDeficit vaporPres;
	
	private SlopeVapourPressure slopeVapPres;
	
	
	private AtmosphericPressure atmPress;
	
	private PsychometricConstant psyco;
	
	private WindSpeed windSpeed;
	
	
	/**
	 * 
	 * @param temp
	 * @param city
	 */
	public ReferenceEvapotraspiration(Temperature temp, City city, WindSpeed windSpeed) {
		this.temp = temp;
		this.city = city;
		this.windSpeed = windSpeed;
		
		vaporPres = new VaporPressureDeficit(temp);
		radiation = new NetRadiation(temp, city);
		slopeVapPres = new SlopeVapourPressure(temp);
		atmPress = new AtmosphericPressure(city);
		psyco = new PsychometricConstant();

	}
	
	
	
	
	
	
	public double getETo() {
		
		boolean debug = false;
		
		////Steps to follows
		//1. Calculate mean air temperature, T [°C]
		double T = temp.getT();
		myDebug(debug, "T " + T);
		//2. Calculate actual vapour pressure, e_a [kPa]
		double e_a = vaporPres.getE_a();
		myDebug(debug, "e_a " + e_a);
		//3. Calculate saturated vapour pressure for Tmax, e_Tmax [kPa] --- vaporPres.getE_Tmax();
		//4. Calculate saturated vapour pressure for Tmin, e_Tmain [kPa] ---- vaporPres.getE_Tmin();
		//5. Calculate saturated vapor pressure, e_s [kPa]
		double e_s = vaporPres.getE_s();
		myDebug(debug, ""+e_s);
		//6. Calculate inverse relative distance Earth-Sun, d_r [rad]
		double d_r = 1 + 0.033 * Math.cos((2 * Math.PI) / 365 * Calendar.getInstance().get(Calendar.DAY_OF_YEAR));
		myDebug(debug, "dr "+ d_r);
		//7. Convert latitude to radians, Phi [rad] - lat is latitude of station in degrees
		double phi_rad = Math.PI / 180 / city.getLat();
		myDebug(debug, "phi_rad "+ phi_rad);
		//8. Calculate solar declination, delta [rad]
		double delta = 0.409 * Math.sin(2 * Math.PI/365 * Calendar.getInstance().get(Calendar.DAY_OF_YEAR) - 1.39);
		myDebug(debug, "delta " + delta);
		//9. Calculate sunset hour angle, Omega_s [rad]
		double omega_s = Math.acos(-1 * Math.tan(phi_rad) * Math.tan(delta));
		myDebug(debug, " " + -1 * Math.tan(phi_rad) * Math.tan(delta));
		myDebug(debug, "omega_s "+ omega_s);
		//10. Calculate extraterrestrial radiation, Ra [MJm-2 day-1]
		double Ra = 24 * 60 / Math.PI * Gsc * d_r * (omega_s * Math.sin(phi_rad) * Math.sin(delta) + Math.cos(phi_rad) * Math.cos(delta) * Math.sin(omega_s));
		myDebug(debug, "Ra " + Ra);
		//11. Calculate clear sky solar radiation, Rso [MJm-2 day -1]
		double Rso = (0.75 + 2 * 0.00001 * getElevationClimStatSea()) * Ra;
		myDebug(debug, "Rso " + Rso);
		//12. Calculate solar radiation,  [MJm-2 day-1] - private radiation.getRs()
		//13. Calculate net longwave radiation, Rnl [MJm-2day-1] - private radiation.getRnl()
		//14. Calculate net solar radiation, Rns [MLm-2 day-1] - private radiation.getRns()
		//15. Calculate net rediation, Rn [MJm-2day-1]
		double Rn = radiation.getRn(Ra, Rso, e_a);
		myDebug(debug, "Rn " + Rn);
		//16. Calculate slope vapour pressure 
		double deltaVap = slopeVapPres.getDelta();
		myDebug(debug, "deltaVap " + deltaVap);
		//17. Calculate atmospheric pressure, P [kPa]
		double P = atmPress.getP();
		myDebug(debug, "P " + P);
		//18. Calculate psychometric costant, y [kPa °C-1]
		double y = psyco.getY(P);
		myDebug(debug, "y " + y);
		//19. Calculate evapotraspitation, ETo
		double u2 = this.windSpeed.getU2();
		myDebug(debug, "u2 " + u2);
		
		double ETo = ((0.408 * deltaVap * Rn + y * (900/T+273) * u2 * (e_s - e_a))/( deltaVap + y * (1 + 0.34 * u2) ));
		
		return ETo;
		
	}
	
	
	private void myDebug(boolean debug, String string) {
		if (debug)
			System.out.println(string);
		
	}
	
	/**
	 * 
	 * @return elevation of climate station above the sea level [m]
	 */
	private double getElevationClimStatSea() {
		
		//it is just an approximate value... 
		return city.getSeaElevation() + 2.5;
	}
	
	
}
