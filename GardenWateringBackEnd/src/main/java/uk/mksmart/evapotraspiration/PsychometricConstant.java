package uk.mksmart.evapotraspiration;



public class PsychometricConstant {
	
	/**
	 * 
	 * @param P - atmospheric pressure, P [kPa]
	 * @return
	 */
	public Double getY(Double P) {
		
		return 0.665 * 0.001 * P;
	}
	
	

}
