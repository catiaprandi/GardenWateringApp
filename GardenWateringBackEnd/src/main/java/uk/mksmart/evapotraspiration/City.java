package uk.mksmart.evapotraspiration;



import java.util.Arrays;
import java.util.List;

public abstract class City {
	
	/**
	 * 0 for Norther
	 * 1 for Souther
	 */
	private boolean hemisphere;
	
	
	private double lat;
	
	
	private double lon;
	
	
	private double seaElevation;
	
	/**
	 * Daily extraterrestrial radiation (Ra) for different latitudes for the 15th day of the month 1
	 */
	// the data for all cities are defined here: http://www.fao.org/docrep/X0490E/x0490e0j.htm, table 2.6
	private List<Double> defaultRa;
	
	/**
	 * 
	 */
	private double Ra;
	
	/**
	 * Mean daylight hours (N) for different latitudes for the 15th of the month1
	 */
	//the data for all cities are defined here: http://www.fao.org/docrep/X0490E/x0490e0j.htm, table 2.7
	private List<Double> N;
	
	/**
	 * position can be coastal (1) or interior (0)
	 * 
	 */
	private boolean position;
	
	
	/**
	 * 0 - Ko = 0 °C for humid and sub-humid climates
	 * 1 - Ko = 2 °C for arid and semi-arid climates
	 */
	private double ko = 0.0;
	
	
	/**
	 * 0.16 for interior locations
	 * 0.19 for coastal locations
	 */
	private double Krs;
	
	
	/**
	 * 
	 * @param hemisphere
	 * @param lat
	 * @param lon
	 * @param seaElevation
	 * @param climateType
	 */
	
	public City (boolean hemisphere, double lat, double lon, double seaElevation, boolean climateType, boolean position) {
		this.hemisphere = hemisphere;
		this.lat = lat;
		this.lon = lon;
		this.seaElevation = seaElevation;
		if(climateType != true)
			ko = 2.0;
		this.position = position;
		if(position == true) //interior location
			Krs = 0.16;
 		else //coastal location
 			Krs = 0.19;
 				
		
	}
	
	public boolean getHemisphere() {
		return hemisphere;
	}
	
	public double getLat() {
		return lat;
	}
	
	public double getLon() {
		return lon;
	}
	
	public double getSeaElevation() {
		return seaElevation;
	}
	
	public List<Double> getN() {
		return N;
	}
	
	public List<Double> getDefaultRa() {
		return defaultRa;
	}
	
	public double getKrs() {
		return Krs;
	}
	
	public double getKo() {
		return ko;
	}
	
	public double getRa() {
		return Ra;
		
	}
	
	public void setRa(double r_a) {
		this.Ra = r_a;
	}
	
	public String getName() {
		return null;
	}
	

}
