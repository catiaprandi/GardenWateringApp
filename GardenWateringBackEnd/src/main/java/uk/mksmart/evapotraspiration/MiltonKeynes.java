package uk.mksmart.evapotraspiration;


import java.util.Arrays;
import java.util.List;


/**
 * TODO
 * buttala tutta via!!! e vai di foglio di configurazione!!!
 * @author catia
 *
 */
public class MiltonKeynes extends City {
	
	

	/**
	 * Daily extraterrestrial radiation (Ra) for different latitudes for the 15th day of the month 1
	 * It can be also calculate (see step 10, Reference Evapotraspiration)
	 */
	// the data for all cities are defined here: http://www.fao.org/docrep/X0490E/x0490e0j.htm, table 2.6
	private List<Double> defaultRa = Arrays.asList(7.7, 13.2, 21.1, 30.8, 38.2, 41.6, 40.1, 33.8, 24.7, 15.7, 9.0, 6.4);
	

	private double r_a;
	
	
	/**
	 * Mean daylight hours (N) for different latitudes for the 15th of the month1
	 */
	//the data for all cities are defined here: http://www.fao.org/docrep/X0490E/x0490e0j.htm, table 2.7
	private  List<Double> N = Arrays.asList(8.0, 9.7, 11.5, 13.6, 15.4, 16.5, 16.0, 14.4, 12.4, 10.3, 8.5, 7.5);
	
	
	/**
	 * 
	 * @param hemisphere - 0 Norther
	 * @param lat - 52
	 * @param lon - 0
	 * @param seaElevation - 114
	 * @param climateType - 
	 * @param postion - 
	 */

	public MiltonKeynes(boolean hemisphere, double lat, double lon, double seaElevation, boolean climateType, boolean position) {
		super(hemisphere, lat, lon, seaElevation, climateType, position);
		// TODO Auto-generated constructor stub
	}
	
	
	public String getName()  {
		return "Milton Keynes";
	}

	
	
	
}
