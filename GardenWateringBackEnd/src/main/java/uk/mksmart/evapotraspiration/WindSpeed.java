package uk.mksmart.evapotraspiration;


public class WindSpeed {

	private Double u2;
	
	
	/**
	 * Class constructor for wind speed at 2 m high [m/s] = 2 m/s (u2)
	 * @param windSpeed
	 */
	public WindSpeed(Double windSpeed) {
		this.u2 = windSpeed;
		
	}
	
	public Double getU2()  {
		return u2;
	}
}
