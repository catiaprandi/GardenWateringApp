package uk.mksmart.kmi.gardenwateringapp;

/**
 * Created by catia on 03/09/15.
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import android.util.Log;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SmartCalendar {

   private ItemsList[] list;


    public ItemsList[] getList() {
        return list;
    }



}
