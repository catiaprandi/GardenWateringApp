package uk.mksmart.kmi.gardenwateringapp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import android.util.Log;

/**
 * Created by catia on 08/09/15.
 */

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemsList {

    private Item[] list;


    public Item[] getList() {
        return list;
    }



    public String getItemValue(String name) {

        for(int i=0; i<list.length; i++){
            Log.d("date", list[i].getName());
            if(list[i].getName().equalsIgnoreCase(name)) {
                return list[i].getValue();
            }
        }

        return null;
    }



}
