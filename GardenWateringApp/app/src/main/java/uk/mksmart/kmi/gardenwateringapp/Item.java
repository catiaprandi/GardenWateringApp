package uk.mksmart.kmi.gardenwateringapp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by catia on 08/09/15.
 */

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Item {

    private String name;
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
