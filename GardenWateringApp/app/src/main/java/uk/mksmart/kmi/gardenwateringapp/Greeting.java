package uk.mksmart.kmi.gardenwateringapp;

/**
 * Created by catia on 17/08/15.
 */

public class Greeting {

    private String id;
    private String content;

    public String getId() {
        return this.id;
    }

    public String getContent() {
        return this.content;
    }

}
