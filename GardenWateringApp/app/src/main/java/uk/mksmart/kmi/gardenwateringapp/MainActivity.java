package uk.mksmart.kmi.gardenwateringapp;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class MainActivity extends AppCompatActivity {


    //public static final int HDR_POS1 = 0;
    //public static final int HDR_POS2 = 1;

    public SmartCalendar smartCalendarMain;

    public static final String[] LIST = { "Today", "MON", "Next days",
            "TUE", "WEN", "THU", "FRI",
            "SAT", "SUN" };

    public static final String[] SUBTEXTS = { null, "He was fond of pulling books off shelves",
            "Made famous by the tunnel of goats", "Claws as big as cups and four arses", "Stressed out sheep", null,
            "Not a racist", "Is there anything to be said for another mass", "Off to get some heroin", "Toddy tod tod. There you are now Todd." };

    private static final Integer LIST_HEADER = 0;
    private static final Integer LIST_ITEM = 1;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       try {
           this.smartCalendarMain = new HttpRequestTaskCurrent().execute().get();
           Log.d("Query", smartCalendarMain.toString());
        } catch (Exception e) {

        }
        setContentView(R.layout.activity_main);


        ListView lv = (ListView)findViewById(R.id.listView1);
        lv.setAdapter(new MyListAdapter(this));
    }


    @Override
    protected void onStart() {
        super.onStart();
        /*try {
            this.smartCalendarMain = new HttpRequestTask().execute().get();
        } catch (Exception e) {

        }*/
        //smartCalendar = new HttpRequestTask().getSmartCalendar();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            try {
                this.smartCalendarMain = new HttpRequestTaskHistory().execute().get();
                Log.d("Query", smartCalendarMain.toString());
            } catch (Exception e) {

            }

            setContentView(R.layout.activity_main);


            ListView lv = (ListView)findViewById(R.id.listView1);
            lv.setAdapter(new MyListAdapter(this));

            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private class HttpRequestTaskCurrent extends AsyncTask<Void, Void, SmartCalendar> {



        @Override
        protected SmartCalendar doInBackground(Void... params) {
            try {
                //final String url = "http://localhost:8080/smartcalendar";

                final String url = "http://10.0.2.2:8080/smartcalendar";
                //final String url = "http://137.108.123.57:8080/smartcalendar";

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                //Greeting greeting = restTemplate.getForObject(url, Greeting.class);
                SmartCalendar smartCalendar = restTemplate.getForObject(url, SmartCalendar.class);

                //smartCalendarMain = smartCalendar;
                return smartCalendar;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }



       /* @Override
        protected void onPostExecute(Greeting greeting) {
            //TODO
            /*TextView greetingIdText = (TextView) findViewById(R.id.id_value);
            TextView greetingContentText = (TextView) findViewById(R.id.content_value);
            greetingIdText.setText(greeting.getId());
            greetingContentText.setText(greeting.getContent());*/
        //ListView lv = (ListView)findViewById(R.id.listView1);
        //lv.setAdapter(new MyListAdapter(this));

        // }

    }



    private class HttpRequestTaskHistory extends AsyncTask<Void, Void, SmartCalendar> {



        @Override
        protected SmartCalendar doInBackground(Void... params) {
            try {
                //final String url = "http://localhost:8080/smartcalendar";

                final String url = "http://10.0.2.2:8080/smartcalendarHistory";
                //final String url = "http://137.108.123.57:8080/smartcalendar";

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                //Greeting greeting = restTemplate.getForObject(url, Greeting.class);
                SmartCalendar smartCalendar = restTemplate.getForObject(url, SmartCalendar.class);

                //smartCalendarMain = smartCalendar;
                return smartCalendar;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }



       /* @Override
        protected void onPostExecute(Greeting greeting) {
            //TODO
            /*TextView greetingIdText = (TextView) findViewById(R.id.id_value);
            TextView greetingContentText = (TextView) findViewById(R.id.content_value);
            greetingIdText.setText(greeting.getId());
            greetingContentText.setText(greeting.getContent());*/
            //ListView lv = (ListView)findViewById(R.id.listView1);
            //lv.setAdapter(new MyListAdapter(this));

        // }

    }

    private class MyListAdapter extends BaseAdapter {
        public MyListAdapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            return smartCalendarMain.getList().length;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
     public View getView(int position, View convertView, ViewGroup parent) {

           /* String headerText = getHeader(position);
            if(headerText != null) {

                View item = convertView;
                if(convertView == null || convertView.getTag() == LIST_ITEM) {

                    item = LayoutInflater.from(mContext).inflate(
                            R.layout.header_list, parent, false);
                    item.setTag(LIST_HEADER);

                }

                TextView headerTextView = (TextView)item.findViewById(R.id.lv_list_hdr);
                //headerTextView.setText("Smart Calendar");
                headerTextView.setText(headerText);
                return item;
            }

            if(position > 1)
                position =  position -1;

            */


            int count=0;


            View item = convertView;
            if(convertView == null || convertView.getTag() == LIST_HEADER) {
                item = LayoutInflater.from(mContext).inflate(
                        R.layout.element_list, parent, false);
                item.setTag(LIST_ITEM);
            }

            Log.d("position", String.valueOf(position));
            TextView header = (TextView)item.findViewById(R.id.lv_item_header);
            //header.setText(LIST[position % LIST.length]);
            String date = smartCalendarMain.getList()[position].getItemValue("date");
            String[] split = date.split(" ");
            //header.setText(split[0].toUpperCase());// +", "+ split[1] + " " + split[2]);
            header.setText(split[0].toUpperCase() +", "+ split[1] + " " + split[2]);
            TextView currentTemp = (TextView)item.findViewById(R.id.currentTemp);





            if(position == 0) {
                header.setTextColor(Color.parseColor("#39b54a"));

                String dayTemp = smartCalendarMain.getList()[position].getItemValue("dayTemp");
                //Log.d("data **********", dayTemp);
                int datTempInt = 0;
                try {
                    datTempInt = ((int) Double.parseDouble(dayTemp));
                } catch (NumberFormatException e)
                {
                    //not a double
                    System.err.print("Error found here.");
                }

                currentTemp.setText(datTempInt + "°C");
                currentTemp.setTextColor(Color.parseColor("#39b54a"));
                currentTemp.setVisibility(View.VISIBLE);
            } else {
                header.setTextColor(Color.parseColor("#FFFFFF"));
                currentTemp.setVisibility(View.INVISIBLE);
            }







            //TextView dayTemp = (TextView)item.findViewById(R.id.day_temp_view);
            //dayTemp.setText(smartCalendarMain.getList()[position].getItemValue("dayTemp").substring(0, 2) + "°C");

            TextView minTemp = (TextView)item.findViewById(R.id.min_temp_textview);

            String minTempString = smartCalendarMain.getList()[position].getItemValue("minTemp");
            int minTempInt = 0;
            try {
                minTempInt = ((int) Double.parseDouble(minTempString));
            } catch (NumberFormatException e)
            {
                //not a double
                System.err.print("Error found here.");
            }
            minTemp.setText(minTempInt + "°C");


            TextView maxTemp = (TextView)item.findViewById(R.id.max_temp_textview);
            String maxTempString = smartCalendarMain.getList()[position].getItemValue("maxTemp");

            int maxTempInt = 0;
            try{
                maxTempInt = ((int) Double.parseDouble(maxTempString));
            } catch (NumberFormatException e)
            {
                //not a double
                System.err.print("Error found here.");
            }


            maxTemp.setText(maxTempInt + "°C");



            String smValueString = smartCalendarMain.getList()[position].getItemValue("SM(real)");



            int smValue = 0;
            try {
                smValue = ((int) Double.parseDouble(smValueString));
                Log.d("valore", "" + smValue);

            } catch(NumberFormatException e)
            {
                //not a double
                System.err.print("Error found here.");
            }





            //////////////////////////////////
            //Debug for DEMO!
            //smValue = smValue;
           // count=count+2;


            //TODO -> non dovrebbe mai diventare 100 ma lo diventa.. quindi ho messo questo solo per demo
         //   if(smValue == 10 || smValue > 95)
         //       smValue = 95;



            //TextView sm = (TextView)item.findViewById(R.id.sm_textview);
            //sm.setText(smValue + "%");

            ImageView smPot = (ImageView)item.findViewById(R.id.smPotView);
            TextView sm = (TextView)item.findViewById(R.id.smValue);
            sm.setText(smValue + "%");


            if(smValue <= 45) {
                smPot.setImageResource(R.mipmap.ic_pot_20plus);

            } else if(smValue <=60) {
                    smPot.setImageResource(R.mipmap.ic_pot_50plus);

            } else if(smValue >= 60 && smValue < 80) {
                        smPot.setImageResource(R.mipmap.ic_pot_70plus);
            } else {
                smPot.setImageResource(R.mipmap.ic_pot_100);
            }



            ImageView imageViewWatering = (ImageView)item.findViewById(R.id.imageViewWatering);
           // TextView labelViewWeather = (TextView)item.findViewById(R.id.labelViewWeather);

            /////////////////////////////////
            //Threshold
            if(smValue > 45 && smValue < 60) {
                imageViewWatering.setImageResource(R.mipmap.ic_light_watering_can);
               // labelViewWeather.setText("Light watering");


                //consigliato annaffiare
            } else if(smValue <= 45) {
                //annaffiare
                imageViewWatering.setImageResource(R.mipmap.ic_water_watering_can);
               // labelViewWeather.setText("Urgent watering");

            } else {
                imageViewWatering.setImageResource(R.mipmap.ic_no_watering_can);
               // labelViewWeather.setText("No watering");
                //nulla

            }


            ImageView imageViewWeather = (ImageView)item.findViewById(R.id.imageViewWeather);
            //TextView labelViewWatering = (TextView)item.findViewById(R.id.labelViewWatering);

            String weatherCond =  smartCalendarMain.getList()[position].getItemValue("description");
            if(weatherCond.equalsIgnoreCase("sky is clear")) {
                imageViewWeather.setImageResource(R.mipmap.ic_sunshine);
               // labelViewWatering.setText("clear sky");

            } else if(weatherCond.equalsIgnoreCase("few clouds")) {
                imageViewWeather.setImageResource(R.mipmap.ic_part_sunshine);
               // labelViewWatering.setText("few clouds");


            } else if(weatherCond.equalsIgnoreCase("scattered clouds") || weatherCond.equalsIgnoreCase("broken clouds")) {
                imageViewWeather.setImageResource(R.mipmap.ic_overcast);
               // labelViewWatering.setText("scattered clouds");



            } else if(weatherCond.equalsIgnoreCase("heavy intensity rain")) {
                imageViewWeather.setImageResource(R.mipmap.ic_heavy_rain);
               // labelViewWatering.setText("heavy rain");

            } else if(weatherCond.equalsIgnoreCase("moderate rain")) {
                imageViewWeather.setImageResource(R.mipmap.ic_light_rain);
                // labelViewWatering.setText("moderate rain");

            } else if(weatherCond.equalsIgnoreCase("light rain")) {
                imageViewWeather.setImageResource(R.mipmap.ic_unsettled);
               // labelViewWatering.setText("light rain");
            }





            //ImageView imageViewWeather = (ImageView)item.findViewById(R.id.imageViewWeather);
            //imageViewWeather.setImageResource(R.mipmap.ic_light_watering_singola);

            //Log.d("data", smartCalendarMain.getList()[position].getItemValue("date"));
            /*TextView subtext = (TextView)item.findViewById(R.id.lv_item_subtext);
            subtext.setText(SUBTEXTS[position % SUBTEXTS.length]);*/

            //Set last divider in a sublist invisible
            View divider = item.findViewById(R.id.item_separator);
         /* if(position == HDR_POS2 -1 ) {
                Log.d("no linea", "******************************" + position);
                divider.setVisibility(View.INVISIBLE);
            } else {
              Log.d("SI linea", "---------------------" + position);
              divider.setVisibility(View.VISIBLE);
          }*/

            return item;
        }

       /* private String getHeader(int position) {

            if(position == HDR_POS1) {
                return "TODAY";
            } else if(position == HDR_POS2) {
                return "NEXT DAYS";
                //return LIST[position];

            }

            return null;
        }*/

        private final Context mContext;
    }

}
